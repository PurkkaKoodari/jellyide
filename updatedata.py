from html import unescape
from os.path import join, dirname, abspath
from typing import Union

from mistletoe import Document
from mistletoe.block_token import Table, BlockToken
from mistletoe.span_token import SpanToken

from jelly import interpreter

IGNORE = [*"0123456789“”»‘’", "<newline> or ¶", "<code>\xA0</code>"]


def text(node: Union[BlockToken, SpanToken]):
    if hasattr(node, "content"):
        return unescape(node.content).replace("THIS_IS_REALLY_A_PIPE", "|")
    return "".join(text(node) for node in node.children)


def update(prevdata, intdata, wikifile):
    with open(join(dirname(abspath(__file__)), "jelly.wiki", wikifile)) as stream:
        contents = stream.read()
        # work around mistletoe parsing
        contents = contents.replace("\\|", "THIS_IS_REALLY_A_PIPE")
        wikitree = Document(contents)

    wikidata = {}
    for table in wikitree.children:
        if isinstance(table, Table):
            for row in table.children:
                name = text(row.children[0])
                wikidata[name] = {
                    "desc": text(row.children[1]).strip()
                }

    allnames = interpreter.unique([*wikidata.keys(), *prevdata.keys(), *intdata.keys()])
    newdata = {}
    for name in allnames:
        if name in IGNORE:
            continue
        prevdesc = prevdata.get(name, {}).get("desc", "")
        keywords = prevdata.get(name, {}).get("keywords", "")
        newdesc = wikidata.get(name, {}).get("desc", "") or prevdesc
        if newdesc and not prevdesc:
            print(f"Added description for {name}: {repr(newdesc)}")
        elif newdesc != prevdesc:
            print(f"Changed description for {name}: {repr(prevdesc)} -> {repr(newdesc)}")
        if not newdesc:
            print(f"Missing description for {name}")
        if not keywords:
            print(f"Missing keywords for {name}")
        if name not in wikidata:
            print(f"{name} not found on wiki page")
        newdata[name] = {
            "desc": newdesc,
            "keywords": keywords,
        }
    return newdata


def quote(string, triple=False):
    quoted = string.replace('\\', '\\\\')
    if triple:
        quoted = quoted.replace('"""', '\\"""')
    else:
        quoted = quoted.replace('"', '\\"')
    return f'"""{quoted}"""' if triple else f'"{quoted}"'


def output_dict(data):
    out = "{\n"
    for name, details in data.items():
        out += f'''\
    {quote(name)}: {{
        "desc": {quote(details["desc"], True)},
        "keywords": {quote(details["keywords"])}
    }},
'''
    out += "}"
    return out


datafile = join(dirname(abspath(__file__)), "jellyide", "data.py")

with open(datafile) as stream:
    current = stream.read()

variables = {}
exec(current, variables)

newlinks = update(variables["ATOMS"], interpreter.atoms, "Atoms.md")
newquicks = update(variables["QUICKS"], {**interpreter.quicks, **interpreter.hypers}, "Quicks.md")
newsyntax = update(variables["SYNTAX"], {}, "Syntax.md")

with open(datafile, "w") as stream:
    stream.write(f"""\
ATOMS = {output_dict(newlinks)}

QUICKS = {output_dict(newquicks)}

SYNTAX = {output_dict(newsyntax)}

ALL = {{**ATOMS, **QUICKS, **SYNTAX}}
""")
