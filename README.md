# Jelly IDE

This is an IDE for the [Jelly](https://github.com/DennisMitchell/jellylanguage) code golfing language.

**Features:**

- Tkinter-based GUI
- Augmented Jelly CLI with most of the features below
- Code editor supporting UTF-8 and Jelly codepage
- Code suggestion with keyword search (press Ctrl+Space)
- Code visualization (links, quicks, chains)
    - Block colors indicate arity of link/chain
    - Bunched-together blocks indicate chain execution steps (+1, 1+, +F and so on)
- Windowed output console
- Step-by-step debugging
    - Break/continue (via GUI or using Ctrl-C in CLI)
    - Start program with stepping (via GUI or `--step` in CLI)
    - Step to next entry/return of an atom/quick
    - Step to return of current atom/quick
    - Shows args or return value in console
- Clean error stack traces with Jelly code locations
- **Notably missing:** command line argument specification in GUI

## Installation & running

To run JellyIDE, you need to have Jelly installed in your virtualenv.

```shell
git clone --recursive https://gitlab.com/PurkkaKoodari/jellyide
cd jellyide
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```

Due to Tkinter limitations, you'll need to install the DejaVu Sans Mono font on the system if you don't already have
it. The font is found in the `jellyide` folder or at [dejavu-fonts.github.io](https://dejavu-fonts.github.io/).

To run the graphical IDE, run `python -m jellyide [filename]`.

To run Jelly's command line, run `python -m jellyide.jelly [--opts] <args...>`. See `--help` for more.

## Implementation

The IDE relies on heavily monkey-patching the core functions in `interpreter.py`, like `parse_code()`,
`*adic_link()` and `*adic_chain()`.

`output()` is hooked to redirect print output to the IDE console.

When running code under the IDE, all parsed code entities are given names for use in visualization, debugging and
stack traces.

`*adic_link()` and `*adic_chain()` catch all errors thrown and convert them into `JellyException`, collecting
stack trace entries there. Furthermore, all `*adic_link()` calls check for the debugger before and after execution.

## License

JellyIDE is licensed under the MIT license.

Marked parts of `debugger.py` and `ide.py` are adapted from the original Jelly source code by DennisMitchell
and are licensed under the MIT license.

The repository includes the DejaVu Sans Mono font, licensed under
a [free license](https://dejavu-fonts.github.io/License.html).
