from sys import argv

from jellyide.debugger import inject_names
from jellyide.ide import JellyIde

inject_names()
JellyIde(argv).ui.mainloop()
