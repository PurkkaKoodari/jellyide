from sys import stderr, stdout, argv
from signal import signal, SIGINT

from jellyide.debugger import DebugState, JellyException, StopRunning, format_jelly_exception, inject_names, Debugger

# Provide minimal debugger functionality for the CLI.

class CliIde:
    def __init__(self):
        self.debugger = Debugger(self)
        self.debugger.hook(hook_output=False)
        self.debugger.start()

    @staticmethod
    def output_callback(text):
        stdout.write(text)
        stdout.flush()

    def set_debug_state(self, state):
        if state != DebugState.running:
            options = [("s", "[s]tep"), ("c", "[c]ontinue"), ("q", "[q]uit")]
            if state == DebugState.paused_call:
                options.insert(1, ("r", "step to [r]eturn"))
            while True:
                stdout.write(", ".join(desc for _, desc in options) + "? ")
                stdout.flush()
                choice = input().lower()[:1]
                if choice in ("", "s"):
                    self.debugger.step()
                    break
                elif choice == "c":
                    self.debugger.resume()
                    break
                elif choice == "r":
                    self.debugger.step_to_return()
                    break
                elif choice == "q":
                    self.debugger.stop()
                    break

inject_names()
ide = CliIde()

# Install sigint handler.

def sigint(_signum, _frame):
    if ide.debugger.paused:
        old_sigint(_signum, _frame)
    else:
        ide.debugger.pause()

old_sigint = signal(SIGINT, sigint)

# Parse --args. Jelly doesn't use them by itself.

usage = """Usage:

    python -m jellyide.jelly [-s] f[u][n] <file> [input...]
    python -m jellyide.jelly [-s] e[u][n] <code> [input...]

JellyIDE adds some options to Jelly, which must come before Jelly's options.

    -h --help    prints this help
    -s --step    starts the debugger at program start

You can press Ctrl-C at any time to interrupt the Jelly program and enter
the debugger.

Jelly's own options:

    jelly f <file> [input]    Reads the Jelly program stored in the
                              specified file, using the Jelly code page.
                              This option should be considered the default,
                              but it exists solely for scoring purposes in
                              code golf contests.

    jelly fu <file> [input]   Reads the Jelly program stored in the
                              specified file, using the UTF-8 encoding.

    jelly e <code> [input]    Reads a Jelly program as a command line
                              argument, using the Jelly code page. This
                              requires setting the environment variable
                              LANG (or your OS's equivalent) to en_US or
                              compatible.

    jelly eu <code> [input]   Reads a Jelly program as a command line
                              argument, using the UTF-8 encoding. This
                              requires setting the environment variable
                              LANG (or your OS's equivalent) to en_US.UTF8
                              or compatible.

  Append an `n` to the flag list to append a trailing newline to the
  program's output.

More information:
  https://gitlab.com/PurkkaKoodari/jellyide
  https://github.com/DennisMitchell/jellylanguage
"""

while len(argv) > 1 and argv[1].startswith("-"):
    arg = argv.pop(1)
    if arg in ("-s", "--step"):
        ide.debugger.pause()
    elif arg in ("-h", "--help"):
        raise SystemExit(usage)
    else:
        raise SystemExit(usage)

# Call Jelly.

try:
    import jelly.__main__
except JellyException as ex:
    print(format_jelly_exception(ex), file=stderr)
except StopRunning:
    pass
except SystemExit as ex:
    if str(ex).startswith("Usage:\n"):
        raise SystemExit(usage)
    raise
