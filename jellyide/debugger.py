import threading
from enum import IntEnum
from traceback import format_exception_only

from jelly import interpreter, attrdict

from jellyide.util import safe_repr


class StopRunning(BaseException):
    pass


class DebugState(IntEnum):
    running = 0
    paused_call = 1
    paused_return = 2


class JellyException(Exception):
    def __init__(self, cause, link=None, chain=None, index=None, args=[]):
        super().__init__(cause)
        self.stack = [(link, chain, index, args)]

    def append(self, link, chain, index, args):
        self.stack.append((link, chain, index, args))


def debugger_step(arity):
    def wrap(func):
        def wrapped(self, link, *args, chain=None, index=None, internal=False, **kwargs):
            stop_at_return = False
            if not internal and self.paused and self.keep_running:
                if arity == 0:
                    trace = ""
                elif arity == 1:
                    trace = "arg: " + safe_repr(args[0]) + "\n"
                else:
                    trace = "left arg: " + safe_repr(args[0][0]) + "\nright arg: " + safe_repr(args[0][1]) + "\n"
                self.print_trace(link, chain, index, trace)
                stop_at_return = self.wait_step(DebugState.paused_call)

            self.stack.append(link)
            try:
                ret = func(self, link, *args, chain=chain, index=index, **kwargs)
            finally:
                assert self.stack.pop() is link

            if stop_at_return:
                self.paused = True

            if not internal and self.paused and self.keep_running:
                trace = "return: " + safe_repr(ret) + "\n"
                self.print_trace(link, chain, index, trace)
                self.wait_step(DebugState.paused_return)

            return ret
        return wrapped
    return wrap


class Debugger:
    active_debugger = None

    def __init__(self, ide):
        self.ide = ide
        self.hooked = None
        self.runner = None
        self.keep_running = True
        self.cond = threading.Condition()
        self.paused = False
        self.stepping = False
        self.stack = []
        self.stepping_to_return = False

    def start(self):
        with self.cond:
            self.keep_running = True
            self.stepping_to_return = False
            self.stack.clear()

    def pause(self):
        with self.cond:
            self.stepping = True
            self.paused = True
            self.cond.notify_all()

    def step(self):
        with self.cond:
            self.stepping = True
            self.paused = False
            self.cond.notify_all()

    def resume(self):
        with self.cond:
            self.stepping = False
            self.paused = False
            self.cond.notify_all()

    def stop(self):
        with self.cond:
            self.keep_running = False
            self.cond.notify_all()

    def step_to_return(self):
        with self.cond:
            if self.keep_running:
                self.stepping_to_return = True
                self.resume()

    def hook(self, hook_output=True):
        if Debugger.active_debugger:
            Debugger.active_debugger.unhook()
        Debugger.active_debugger = self
        self.hooked = {}
        names = ["niladic_chain", "monadic_chain", "dyadic_chain", "niladic_link", "monadic_link", "dyadic_link"]
        if hook_output:
            names += ["output"]
        for method in names:
            self.hooked[method] = getattr(interpreter, method)
            setattr(interpreter, method, getattr(self, method))

    def unhook(self):
        if self.hooked:
            for method in self.hooked:
                setattr(interpreter, method, self.hooked[method])
        self.hooked = None
        Debugger.active_debugger = None

    def wait_step(self, state):
        with self.cond:
            if self.keep_running and self.paused:
                self.ide.set_debug_state(state)
                self.cond.wait_for(lambda: not self.keep_running or not self.paused)
                self.ide.set_debug_state(DebugState.running)
            if self.stepping:
                self.paused = True
            if self.stepping_to_return:
                self.stepping_to_return = False
                if state != DebugState.paused_call:
                    self.paused = True
                    return False
                return True
            return False

    def print_trace(self, link, chain, index, message):
        trace = "--- PAUSED ---\n"
        chain, index = self.determine_caller(link, chain, index, depth=1)
        if index is not None:
            trace += "".join(chain) + "\n"
            trace += " " * sum(len(name) for name in chain[:index]) + "^" * len(chain[index]) + "\n"
        elif chain is not None:
            trace += "in " + getattr(link, "fullname", "???") + " (called by " + "".join(chain) + ")\n"
        else:
            trace += "in " + getattr(link, "fullname", "???") + " (caller unknown)\n"
        trace += message
        self.ide.output_callback(trace)

    def determine_caller(self, link, chain, index, depth=2):
        # if we're within a chain, we already know the caller
        if chain:
            return chain, index
        # otherwise, look in the stack (by default ignore stack[-1] which is this link)
        if len(self.stack) >= depth:
            caller = self.stack[-depth]
            # is this a quick/hyper?
            if hasattr(caller, "children"):
                index = caller.children.index(link) if link in caller.children else None
                names = [getattr(link, "fullname", "???") for link in caller.children] + [caller.name]
                return names, index
            return [caller.fullname], None
        # no can do
        return chain, index

    def output(self, argument, end='', transform=interpreter.stringify):
        self.ide.output_callback(str(transform(argument)) + end)
        return argument

    # The following methods are modified from the original Jelly source code by DennisMitchell.

    def niladic_chain(self, chain):
        while len(chain) == 1 and hasattr(chain[0], 'chain'):
            chain = chain[0].chain
        if not self.keep_running:
            raise StopRunning()
        if not chain or chain[0].arity > 0:
            return self.monadic_chain(chain, 0, prev_chain=[])
        names = [getattr(link, "fullname", "???") for link in chain]
        tmp_larg = self.niladic_link(chain[0], chain=names, index=0)
        return self.monadic_chain(chain[1:], tmp_larg, prev_chain=names[:1])

    def monadic_chain(self, chain, arg, prev_chain=None):
        init = True
        ret = arg
        names = prev_chain or []
        index = len(names)
        while True:
            if not self.keep_running:
                raise StopRunning()
            if init:
                names.extend(getattr(link, "fullname", "???") for link in chain)
                for link in chain:
                    if link.arity < 0:
                        link.arity = 1
                if interpreter.leading_nilad(chain):
                    ret = self.niladic_link(chain[0], chain=names, index=index)
                    chain = chain[1:]
                    index += 1
                init = False
            if not chain:
                break
            if interpreter.arities(chain[0:2]) == [2, 1]:
                tmp_rarg = self.monadic_link(chain[1], arg, chain=names, index=index + 1)
                ret = self.dyadic_link(chain[0], (ret, tmp_rarg), chain=names, index=index)
                chain = chain[2:]
                index += 2
            elif interpreter.arities(chain[0:2]) == [2, 0]:
                tmp_rarg = self.niladic_link(chain[1], chain=names, index=index + 1)
                ret = self.dyadic_link(chain[0], (ret, tmp_rarg), chain=names, index=index)
                chain = chain[2:]
                index += 2
            elif interpreter.arities(chain[0:2]) == [0, 2]:
                tmp_larg = self.niladic_link(chain[0], chain=names, index=index)
                ret = self.dyadic_link(chain[1], (tmp_larg, ret), chain=names, index=index + 1)
                chain = chain[2:]
                index += 2
            elif chain[0].arity == 2:
                ret = self.dyadic_link(chain[0], (ret, arg), chain=names, index=index)
                chain = chain[1:]
                index += 1
            elif chain[0].arity == 1:
                if not chain[1:] and hasattr(chain[0], 'chain'):
                    arg = ret
                    chain = chain[0].chain
                    interpreter.atoms['⁸'].call = lambda literal = arg: literal
                    init = True
                    names.pop(index)
                else:
                    ret = self.monadic_link(chain[0], ret, chain=names, index=index)
                    if hasattr(chain[0], "chain"):
                        names.pop(index)
                    else:
                        index += 1
                    chain = chain[1:]
            else:
                interpreter.output(ret)
                ret = self.niladic_link(chain[0], chain=names, index=index)
                chain = chain[1:]
                index += 1
        return ret

    def dyadic_chain(self, chain, args):
        larg, rarg = args
        for link in chain:
            if link.arity < 0:
                link.arity = 2
        if not self.keep_running:
            raise StopRunning()
        names = [getattr(link, "fullname", "???") for link in chain]
        index = 0
        if chain and interpreter.arities(chain[0:3]) == [2, 2, 2]:
            ret = self.dyadic_link(chain[0], args, chain=names, index=index)
            chain = chain[1:]
            index += 1
        elif interpreter.leading_nilad(chain):
            ret = self.niladic_link(chain[0], chain=names, index=index)
            chain = chain[1:]
            index += 1
        else:
            ret = larg
        while chain:
            if not self.keep_running:
                raise StopRunning()
            if interpreter.arities(chain[0:3]) == [2, 2, 0] and interpreter.leading_nilad(chain[2:]):
                tmp_larg = self.dyadic_link(chain[0], (ret, rarg), chain=names, index=index)
                tmp_rarg = self.niladic_link(chain[2], chain=names, index=index + 2)
                ret = self.dyadic_link(chain[1], (tmp_larg, tmp_rarg), chain=names, index=index + 1)
                chain = chain[3:]
                index += 3
            elif interpreter.arities(chain[0:2]) == [2, 2]:
                tmp_rarg = self.dyadic_link(chain[1], args, chain=names, index=index + 1)
                ret = self.dyadic_link(chain[0], (ret, tmp_rarg), chain=names, index=index)
                chain = chain[2:]
                index += 2
            elif interpreter.arities(chain[0:2]) == [2, 0]:
                tmp_rarg = self.niladic_link(chain[1], chain=names, index=index + 1)
                ret = self.dyadic_link(chain[0], (ret, tmp_rarg), chain=names, index=index)
                chain = chain[2:]
                index += 2
            elif interpreter.arities(chain[0:2]) == [0, 2]:
                tmp_larg = self.niladic_link(chain[0], chain=names, index=index)
                ret = self.dyadic_link(chain[1], (tmp_larg, ret), chain=names, index=index + 1)
                chain = chain[2:]
                index += 2
            elif chain[0].arity == 2:
                ret = self.dyadic_link(chain[0], (ret, rarg), chain=names, index=index)
                chain = chain[1:]
                index += 1
            elif chain[0].arity == 1:
                ret = self.monadic_link(chain[0], ret, chain=names, index=index)
                chain = chain[1:]
                index += 1
            else:
                self.output(ret)
                ret = self.niladic_link(chain[0], chain=names, index=index)
                chain = chain[1:]
                index += 1
        return ret

    @debugger_step(0)
    def niladic_link(self, link, chain=None, index=None):
        try:
            return link.call()
        except JellyException as ex:
            chain, index = self.determine_caller(link, chain, index)
            ex.append(link, chain, index, [])
            raise
        except Exception as ex:
            chain, index = self.determine_caller(link, chain, index)
            raise JellyException(ex, link, chain, index, [])

    @debugger_step(1)
    def monadic_link(self, link, arg, flat=False, conv=True, internal=False, chain=None, index=None):
        try:
            flat = flat or not hasattr(link, 'ldepth')
            arg_depth = flat or interpreter.depth(arg)
            if flat or link.ldepth == arg_depth:
                if conv and hasattr(link, 'conv'):
                    return link.conv(link.call, arg)
                return link.call(arg)
            conv = conv and hasattr(link, 'conv')
            if link.ldepth > arg_depth:
                return self.monadic_link(link, [arg], conv=conv, internal=True, chain=chain, index=index)
            return [self.monadic_link(link, z, conv=conv, internal=True, chain=chain, index=index) for z in arg]
        except JellyException as ex:
            if not internal:
                chain, index = self.determine_caller(link, chain, index)
                ex.append(link, chain, index, [arg])
            raise
        except Exception as ex:
            chain, index = self.determine_caller(link, chain, index)
            raise JellyException(ex, link, chain, index, [arg])

    @debugger_step(2)
    def dyadic_link(self, link, args, conv=True, lflat=False, rflat=False, internal=False, chain=None, index=None):
        try:
            larg, rarg = args
            lflat = lflat or not hasattr(link, 'ldepth')
            rflat = rflat or not hasattr(link, 'rdepth')
            larg_depth = lflat or interpreter.depth(larg)
            rarg_depth = rflat or interpreter.depth(rarg)
            if (lflat or link.ldepth == larg_depth) and (rflat or link.rdepth == rarg_depth):
                if conv and hasattr(link, 'conv'):
                    return link.conv(link.call, larg, rarg)
                return link.call(larg, rarg)
            conv = conv and hasattr(link, 'conv')
            if not lflat and larg_depth < link.ldepth:
                return self.dyadic_link(link, ([larg], rarg), internal=True, chain=chain, index=index)
            if not rflat and rarg_depth < link.rdepth:
                return self.dyadic_link(link, (larg, [rarg]), internal=True, chain=chain, index=index)
            if not rflat and (lflat or larg_depth - rarg_depth < link.ldepth - link.rdepth):
                return [self.dyadic_link(link, (larg, y), internal=True, chain=chain, index=index) for y in rarg]
            if not lflat and (rflat or larg_depth - rarg_depth > link.ldepth - link.rdepth):
                return [self.dyadic_link(link, (x, rarg), internal=True, chain=chain, index=index) for x in larg]
            return [self.dyadic_link(link, (x, y), internal=True, chain=chain, index=index) for x, y in zip(*args)] + larg[len(rarg) :] + rarg[len(larg) :]
        except JellyException as ex:
            if not internal:
                chain, index = self.determine_caller(link, chain, index)
                ex.append(link, chain, index, args)
            raise
        except Exception as ex:
            chain, index = self.determine_caller(link, chain, index)
            raise JellyException(ex, link, chain, index, args)


def wrap_quick(name, quick):
    if getattr(quick, "ide_wrapper", None):
        return quick
    def wrapper(links, outmost_links, index):
        ret = quick(links[:], outmost_links, index)
        # if multiple links are returned, everything except the last must be ignored inputs
        assert ret[:-1] == links[:len(ret) - 1]
        # if the link already has a name, it has to be a link reference or duplicate
        if hasattr(ret[-1], "name"):
            if ret[-1].name is None:
                # link reference
                ret[-1] = attrdict(**ret[-1])
                ret[-1].name = name
                ret[-1].fullname = name
                del ret[-1].children
            else:
                # duplicated link
                pass
        else:
            ret[-1].children = children = links[len(ret) - 1:]
            ret[-1].name = name
            ret[-1].fullname = "".join(link.fullname for link in children) + name
        return ret
    wrapper.ide_wrapper = True
    return wrapper


def wrap_hyper(name, hyper):
    if getattr(hyper, "ide_wrapper", None):
        return hyper
    def wrapper(link, none):
        ret = hyper(link, none)
        assert not hasattr(ret, "name")
        ret.name = name
        ret.fullname = link.fullname + name
        ret.children = [link]
        return ret
    wrapper.ide_wrapper = True
    return wrapper


def create_chain(chain, arity=-1, isForward=True):
    return attrdict(
        arity=arity,
        chain=chain,
        call=lambda x=None, y=None: interpreter.variadic_chain(chain, isForward and (x, y) or (y, x)),
        name=None,
        fullname="".join(child.fullname for child in chain),
        children=chain
    )


# The following function is modified from the original Jelly source code by DennisMitchell.

def parse_code(code):
    lines = interpreter.regex_flink.findall(code)
    links = [[] for line in lines]
    for index, line in enumerate(lines):
        chains = links[index]
        for word in interpreter.regex_chain.findall(line):
            chain = []
            arity, start, isForward = interpreter.chain_separators.get(word[:1], interpreter.default_chain_separation)
            for token in interpreter.regex_token.findall(start + word):
                if token in interpreter.atoms:
                    chain.append(interpreter.atoms[token])
                elif token in interpreter.quicks:
                    popped = []
                    while not interpreter.quicks[token].condition(popped) and (chain or chains):
                        popped.insert(0, chain.pop() if chain else chains.pop())
                    chain += interpreter.quicks[token].quicklink(popped, links, index)
                elif token in interpreter.hypers:
                    x = chain.pop() if chain else chains.pop()
                    chain.append(interpreter.hypers[token](x, links))
                else:
                    chain.append(interpreter.create_literal(interpreter.regex_liter.sub(interpreter.parse_literal, token)))
                    chain[-1].fullname = chain[-1].name = token
            chains.append(interpreter.create_chain(chain, arity, isForward))
    return links


def inject_names():
    """Injects code into the Jelly interpreter so that all parsed links have names."""
    for name, atom in interpreter.atoms.items():
        atom.fullname = atom.name = name
    for name, quick in interpreter.quicks.items():
        quick.quicklink = wrap_quick(name, quick.quicklink)
    for name, hyper in list(interpreter.hypers.items()):
        interpreter.hypers[name] = wrap_hyper(name, hyper)

    interpreter.create_chain = create_chain
    interpreter.parse_code = parse_code


def format_jelly_exception(ex):
    trace = ""
    for link, chain, index, args in ex.stack:
        trace += "in " + getattr(link, "fullname", "???") + "\n"
        if chain is not None:
            trace += "".join(chain) + "\n"
            trace += " " * sum(len(name) for name in chain[:index]) + "^" * len(chain[index]) + "\n"
            if len(args) == 1:
                trace += "arg: " + safe_repr(args[0]) + "\n"
            elif len(args) >= 2:
                trace += "left arg: " + safe_repr(args[0]) + "\n"
                trace += "right arg: " + safe_repr(args[1]) + "\n"
        trace += "\n"
    cause = ex.args[0]
    return trace + "".join(format_exception_only(type(cause), cause)).rstrip("\n")
