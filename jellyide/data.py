ATOMS = {
    "®": {
        "desc": """Restore; retrieve the value of the register. Initially 0.""",
        "keywords": "restore get register"
    },
    "³": {
        "desc": """Return the third command line argument (first input) or 100.""",
        "keywords": "command line argument third first input hundred"
    },
    "⁴": {
        "desc": """Return the fourth command line argument (second input) or 16.""",
        "keywords": "command line argument fourth second input sixteen"
    },
    "⁵": {
        "desc": """Return the fifth command line argument (third input) or 10.""",
        "keywords": "command line argument fifth third input ten"
    },
    "⁶": {
        "desc": """Return the sixth command line argument (fourth input) or ' '.""",
        "keywords": "command line argument sixth fourth input space"
    },
    "⁷": {
        "desc": """Return the seventh command line argument (fifth input) or '\\n'.""",
        "keywords": "command line argument seventh fifth input newlines"
    },
    "⁸": {
        "desc": """Return the link's left argument or [].""",
        "keywords": "get link left argument [] empty array"
    },
    "⁹": {
        "desc": """Return the link's right argument or 256.""",
        "keywords": "get link right argument 256"
    },
    "Ɠ": {
        "desc": """Read and evaluate a single line from STDIN.""",
        "keywords": "read input stdin line evaluate"
    },
    "ƈ": {
        "desc": """Read a single character from STDIN.""",
        "keywords": "read input stdin character"
    },
    "ɠ": {
        "desc": """Read a single line from STDIN.""",
        "keywords": "read input stdin line raw"
    },
    "¬": {
        "desc": """Logical NOT: if z is zero or empty, then 1, else 0.""",
        "keywords": "logical not invert negate"
    },
    "½": {
        "desc": """Square root.""",
        "keywords": "square root power half second two"
    },
    "!": {
        "desc": """Factorial or Pi function.""",
        "keywords": "factorial gamma pi function"
    },
    "A": {
        "desc": """Absolute value.""",
        "keywords": "absolute value"
    },
    "B": {
        "desc": """Convert from integer to binary.""",
        "keywords": "convert integer binary"
    },
    "C": {
        "desc": """Complement; compute 1 − z.""",
        "keywords": "complement subtraction one"
    },
    "D": {
        "desc": """Convert from integer to decimal.""",
        "keywords": "convert integer decimal digits"
    },
    "E": {
        "desc": """Check if all elements of z are equal.""",
        "keywords": "all equal"
    },
    "F": {
        "desc": """Flatten list.""",
        "keywords": "flatten"
    },
    "G": {
        "desc": """Attempt to format z as a grid.""",
        "keywords": "format matrix grid"
    },
    "H": {
        "desc": """Halve; compute z ÷ 2.""",
        "keywords": "halve half divide two"
    },
    "I": {
        "desc": """Increments; compute the differences of consecutive elements of z.""",
        "keywords": "increments deltas"
    },
    "J": {
        "desc": """Returns [1 … len(z)].""",
        "keywords": "lists arrays indices range"
    },
    "K": {
        "desc": """Join z, separating by spaces.""",
        "keywords": "join spaces"
    },
    "L": {
        "desc": """Length.""",
        "keywords": "lists arrays length"
    },
    "M": {
        "desc": """Return all indices of z that correspond to maximal elements.""",
        "keywords": "maximal indices"
    },
    "N": {
        "desc": """Negative; compute −z.""",
        "keywords": "negative negate"
    },
    "O": {
        "desc": """Ord; cast to number.""",
        "keywords": "character code charcode ordinal cast number integer"
    },
    "P": {
        "desc": """Product of a list.""",
        "keywords": "product muliply multiplication"
    },
    "Q": {
        "desc": """Return the unique elements of z, sorted by first appearance.""",
        "keywords": "unique uniquify deduplicate duplicates list"
    },
    "R": {
        "desc": """Inclusive range [1 … z].""",
        "keywords": "inclusive range"
    },
    "S": {
        "desc": """Sum of a list.""",
        "keywords": "sum addition"
    },
    "T": {
        "desc": """Return all indices of z that correspond to truthy elements.""",
        "keywords": "truthy indices"
    },
    "U": {
        "desc": """Upend; reverse an array.""",
        "keywords": "reverse upend"
    },
    "V": {
        "desc": """Eval z as Jelly code, with no arguments. If z is a list, it maps Python's str function to all of z's elements, concatenates them and then eval's the result. Vectorizes at depth 1.""",
        "keywords": "evaluate jelly code nilad arguments"
    },
    "W": {
        "desc": """Wrap; return [z].""",
        "keywords": "wrap"
    },
    "X": {
        "desc": """Random; choose a random item from z if z is a list, or from 1 to z inclusive if z is a positive integer. If z = 0, return z. Error if z is negative or a decimal.""",
        "keywords": "random choose choice pick range randrange randint"
    },
    "Y": {
        "desc": """Join z, separating by linefeeds.""",
        "keywords": "join newlines"
    },
    "Z": {
        "desc": """Zip; push the array of all columns of z.""",
        "keywords": "zip transpose columns"
    },
    "~": {
        "desc": """Bitwise NOT.""",
        "keywords": "bitwise not"
    },
    "°": {
        "desc": """Convert z from degrees to radians.""",
        "keywords": "convert degrees radians"
    },
    "¹": {
        "desc": """Identity; return z.""",
        "keywords": "identity nop no-op noop nothing"
    },
    "²": {
        "desc": """Square.""",
        "keywords": "square power two second"
    },
    "Ạ": {
        "desc": """All; return 0 if z contains a falsey value, else 1.""",
        "keywords": "all truthy falsey true"
    },
    "Ḅ": {
        "desc": """Convert from binary to integer.""",
        "keywords": "convert binary integer"
    },
    "Ḍ": {
        "desc": """Convert from decimal to integer.""",
        "keywords": "convert decimal digits integer"
    },
    "Ẹ": {
        "desc": """Any; return 1 if z contains a truthy value, else 0.""",
        "keywords": "any truthy falsey true"
    },
    "Ḥ": {
        "desc": """Double; compute 2z.""",
        "keywords": "double multiply two"
    },
    "Ị": {
        "desc": """Insignificant; return abs(z) ≤ 1.""",
        "keywords": "insignificant small absolute"
    },
    "Ḳ": {
        "desc": """Split z at spaces.""",
        "keywords": "split spaces"
    },
    "Ḷ": {
        "desc": """Lowered range; return [0 … z−1].""",
        "keywords": "lowered exclusive range"
    },
    "Ṃ": {
        "desc": """Minimum.""",
        "keywords": "minimum"
    },
    "Ṇ": {
        "desc": """Logical NOT. Does not vectorize.""",
        "keywords": "logical not invert negate"
    },
    "Ọ": {
        "desc": """Chr; cast to character.""",
        "keywords": "chr cast character charcode code number integer"
    },
    "Ṛ": {
        "desc": """Reverse z. Does not vectorize.""",
        "keywords": "reverse"
    },
    "Ṣ": {
        "desc": """Sort the list z.""",
        "keywords": "sorted"
    },
    "Ṭ": {
        "desc": """Return a Boolean array with 1s at the indices in z.""",
        "keywords": "truthy true boolean indices"
    },
    "Ụ": {
        "desc": """Grade the list z up, i.e., sort its indices by their corresponding values.""",
        "keywords": "grade up sort indices"
    },
    "Ṿ": {
        "desc": """Uneval; right inverse of V.""",
        "keywords": "unevaluate representation jelly code"
    },
    "Ẉ": {
        "desc": """Get the length of each element of z.""",
        "keywords": "lists arrays length map each"
    },
    "Ỵ": {
        "desc": """Split z at linefeeds.""",
        "keywords": "split newlines"
    },
    "Ẓ": {
        "desc": """If z is a prime, then 1, else 0 (alias for ÆP).""",
        "keywords": "primes"
    },
    "Ȧ": {
        "desc": """Any and all; return 0 if z is empty, or contains a falsey value when flattened, else 1.""",
        "keywords": "all any truthy falsey true"
    },
    "Ḃ": {
        "desc": """Bit; return z % 2.""",
        "keywords": "last bit modulo two"
    },
    "Ċ": {
        "desc": """Ceil; round z up to the nearest integer. Imag. part for complex z.""",
        "keywords": "ceiling round up integer imaginary part"
    },
    "Ḋ": {
        "desc": """Dequeue; return z[1:].""",
        "keywords": "dequeue tail remove first"
    },
    "Ė": {
        "desc": """Enumerate; return [ [1,y[1]] , [2,y[2]], ... ].""",
        "keywords": "enumerate indices pairs"
    },
    "Ḟ": {
        "desc": """Floor; round z down to the nearest integer. Real part for complex z.""",
        "keywords": "floor round down integer real part"
    },
    "Ġ": {
        "desc": """Group the indices of z by their corresponding values.""",
        "keywords": "group indices values"
    },
    "Ḣ": {
        "desc": """Head; pop and return the first element of z. Modifies z.""",
        "keywords": "head pop first"
    },
    "İ": {
        "desc": """Inverse;Reciprocal; compute 1 ÷ z.""",
        "keywords": "inverse divide one"
    },
    "Ṁ": {
        "desc": """Maximum.""",
        "keywords": "maximum"
    },
    "Ṅ": {
        "desc": """Print z and a linefeed. Returns z.""",
        "keywords": "print output linefeed newline linebreak break"
    },
    "Ȯ": {
        "desc": """Print z. Returns z.""",
        "keywords": "print output"
    },
    "Ṗ": {
        "desc": """Pop; return z[:-1].""",
        "keywords": "pop remove last"
    },
    "Ṙ": {
        "desc": """Print a string representation of z, without a linefeed. Returns z.""",
        "keywords": "print output representation jelly code"
    },
    "Ṡ": {
        "desc": """Sign of z. Conjugate for complex z.""",
        "keywords": "signum conjugate"
    },
    "Ṫ": {
        "desc": """Tail; pop and return the last element of z. Modifies z.""",
        "keywords": "tail pop last"
    },
    "Ẇ": {
        "desc": """Sublists; all non-empty contiguous slices of z.""",
        "keywords": "slices sublists"
    },
    "Ẋ": {
        "desc": """Shuffle; return a random permutation of z.""",
        "keywords": "shuffle random permutations"
    },
    "Ẏ": {
        "desc": """Tighten; dump all lists inside z.""",
        "keywords": "tighten flatten one once"
    },
    "Ż": {
        "desc": """Prepend 0 to z. For integers, return [0 … z].""",
        "keywords": "prepend concatenate zero range inclusive"
    },
    "§": {
        "desc": """Sum. Vectorizes at depth 1.""",
        "keywords": "sum addition"
    },
    "Ä": {
        "desc": """Cumulative sum. Vectorizes at depth 1.""",
        "keywords": "sum addition cumulative"
    },
    "×": {
        "desc": """Multiplication.""",
        "keywords": "multiplication product"
    },
    "÷": {
        "desc": """Floating point division.""",
        "keywords": "division floating point"
    },
    "%": {
        "desc": """Modulus.""",
        "keywords": "modulo remainder"
    },
    "&": {
        "desc": """Bitwise AND.""",
        "keywords": "bitwise and"
    },
    "*": {
        "desc": """Exponentiation with base x.""",
        "keywords": "exponentiation power"
    },
    "+": {
        "desc": """Addition.""",
        "keywords": "addition sum"
    },
    ",": {
        "desc": """Pair; return [x, y].""",
        "keywords": "pairs 2-tuples two-tuples"
    },
    ":": {
        "desc": """Integer division.""",
        "keywords": "division integer"
    },
    ";": {
        "desc": """Concatenate.""",
        "keywords": "concatenate"
    },
    "<": {
        "desc": """If x is less than y, then 1, else 0.""",
        "keywords": "compare less smaller"
    },
    "=": {
        "desc": """If x equals y, then 1, else 0. Vectorizes.""",
        "keywords": "compare equals equality"
    },
    ">": {
        "desc": """If x is greater than y, then 1, else 0.""",
        "keywords": "compare greater larger"
    },
    "^": {
        "desc": """Bitwise XOR.""",
        "keywords": "bitwise xor exclusive or"
    },
    "_": {
        "desc": """Subtraction.""",
        "keywords": "subtraction difference"
    },
    "a": {
        "desc": """Logical AND. Vectorizes with depth 0.""",
        "keywords": "logical and"
    },
    "b": {
        "desc": """Convert from integer to base y.""",
        "keywords": "convert integer base digits"
    },
    "c": {
        "desc": """Combinations; compute xCy.""",
        "keywords": "combinations binomial"
    },
    "d": {
        "desc": """Divmod.""",
        "keywords": "divmod division remainder modulo"
    },
    "e": {
        "desc": """If x occurs in y, then 1, else 0.""",
        "keywords": "exists in contains"
    },
    "f": {
        "desc": """Filter; remove the elements from x that are not in y.""",
        "keywords": "filter contained keep remove"
    },
    "g": {
        "desc": """Greatest common divisor (GCD, HCF).""",
        "keywords": "greatest highest largest common divisors"
    },
    "i": {
        "desc": """Find the first index of element y in list x, or 0.""",
        "keywords": "find first indices"
    },
    "j": {
        "desc": """Join list x with separator y.""",
        "keywords": "join separator glue"
    },
    "k": {
        "desc": """Partition y after truthy indices of x.""",
        "keywords": "split truthy true indices"
    },
    "l": {
        "desc": """Logarithm with base y.""",
        "keywords": "logarithm base"
    },
    "m": {
        "desc": """Modular; return every y th element of x. If y is zero, mirror: prepend x to its reverse.""",
        "keywords": "modular indices"
    },
    "n": {
        "desc": """Not equals. If x equals y, then 0, else 1. Vectorizes.""",
        "keywords": "compare not equals equality inequality"
    },
    "o": {
        "desc": """Logical OR. Vectorizes with depth 0.""",
        "keywords": "logical or"
    },
    "p": {
        "desc": """Cartesian product.""",
        "keywords": "cartesian product"
    },
    "r": {
        "desc": """Inclusive range. Descending if x > y.""",
        "keywords": "inclusive range"
    },
    "s": {
        "desc": """Split x into slices of length y.""",
        "keywords": "split sublists slices length"
    },
    "t": {
        "desc": """Trim all elements of y from both sides of x.""",
        "keywords": "trim both sides"
    },
    "v": {
        "desc": """Eval x as Jelly code, with argument y.""",
        "keywords": "evaluate jelly code argument monad"
    },
    "w": {
        "desc": """Find the first index of sublist y within list x, or 0.""",
        "keywords": "find first indices sublists"
    },
    "x": {
        "desc": """Times; repeat the elements of x y times.""",
        "keywords": "repeat multiplication"
    },
    "y": {
        "desc": """Translate the elements of y according to the mapping in x.""",
        "keywords": "translate transform mapping"
    },
    "z": {
        "desc": """Zip; transpose x with filler y.""",
        "keywords": "zip transpose columns filler filled"
    },
    "|": {
        "desc": """Bitwise OR.""",
        "keywords": "bitwise or"
    },
    "⁼": {
        "desc": """Equals. Does not vectorize.""",
        "keywords": "compare equals equality"
    },
    "⁻": {
        "desc": """Not equals. Does not vectorize.""",
        "keywords": "compare not equals equality inequality"
    },
    "ạ": {
        "desc": """Absolute difference.""",
        "keywords": "absolute value difference subtraction"
    },
    "ḅ": {
        "desc": """Convert from base y to integer.""",
        "keywords": "convert base digits integer"
    },
    "ḍ": {
        "desc": """If x divides y, then 1, else 0.""",
        "keywords": "divisible division remainder modulo divisors"
    },
    "ẹ": {
        "desc": """Return the indices of all occurrences of y in x.""",
        "keywords": "find indices"
    },
    "ḥ": {
        "desc": """Jelly hash function. x contains a magic number (akin to a salt, can also be a base-250 compressed integer as a string) concatenated with a collection of values. If there are more than two items in x, all but the first are concatenated into a single collection. If there are two items and the second is a number N, the range [1,N] is used as the collection. y is the input to the hash function. Depending on the resulting hash, an item from the collection is returned.""",
        "keywords": "hash"
    },
    "ị": {
        "desc": """Return the element of y at index x % len(y). If floor(x) and ceil(x) aren't equal, return the elements of y at indices floor(x) % len(y) and ceil(x) % len(y).""",
        "keywords": "indices"
    },
    "ḷ": {
        "desc": """Left argument; return x.""",
        "keywords": "left argument"
    },
    "ṃ": {
        "desc": """Base decompression; convert x to base length(y) then index into y.""",
        "keywords": "base decompression convert"
    },
    "ọ": {
        "desc": """Order, multiplicity, valuation; how many times is x divisible by y?""",
        "keywords": "order multiplicity valuation divisible division"
    },
    "ṛ": {
        "desc": """Right argument; return y.""",
        "keywords": "right argument"
    },
    "ṣ": {
        "desc": """Split list x at occurrences of y.""",
        "keywords": "split slices separator"
    },
    "ṭ": {
        "desc": """Tack; append x to y.""",
        "keywords": "tack append concatenate"
    },
    "ȧ": {
        "desc": """Logical AND. Does not vectorize.""",
        "keywords": "logical and"
    },
    "ḃ": {
        "desc": """Convert from integer to bijective base y.""",
        "keywords": "convert integer bijective base digits"
    },
    "ċ": {
        "desc": """Count the occurrences of y in x.""",
        "keywords": "count occurrences"
    },
    "ḋ": {
        "desc": """Dot product of two vectors (real/complex). Right pads the shorter argument with 0.""",
        "keywords": "dot product"
    },
    "ḟ": {
        "desc": """Filter; remove the elements from x that are in y.""",
        "keywords": "filter keep remove inverted contained"
    },
    "ḣ": {
        "desc": """Head; return x[:y].""",
        "keywords": "head first"
    },
    "ṁ": {
        "desc": """Mold; reshape the content of x like the content of y. Reuses the content of x if necessary.""",
        "keywords": "mold reshape"
    },
    "ȯ": {
        "desc": """Logical OR. Does not vectorize.""",
        "keywords": "logical or"
    },
    "ṗ": {
        "desc": """Cartesian power.""",
        "keywords": "cartesian power"
    },
    "ṙ": {
        "desc": """Rotate x y units to the left.""",
        "keywords": "rotate left"
    },
    "ṡ": {
        "desc": """Return all (overlapping) slices of length y of x.""",
        "keywords": "slices sublists"
    },
    "ṫ": {
        "desc": """Tail; return x[y - 1:].""",
        "keywords": "tail last"
    },
    "ẇ": {
        "desc": """Sublist exists; return 1 if x is a contiguous sublist of y, else 0.""",
        "keywords": "sublists slices find exists"
    },
    "ẋ": {
        "desc": """Repeat list x y times.""",
        "keywords": "repeat multiplication"
    },
    "ż": {
        "desc": """Zip; interleave x and y.""",
        "keywords": "zip interleave"
    },
    "«": {
        "desc": """Minimum of x and y.""",
        "keywords": "minimum"
    },
    "Ø½": {
        "desc": """Yield [1, 2].""",
        "keywords": "one two constant"
    },
    "Ø%": {
        "desc": """Yield 4294967296 (2<sup>32</sup>).""",
        "keywords": "two 32 4294967296 constant"
    },
    "Ø(": {
        "desc": """Parentheses; yield '()'.""",
        "keywords": "parentheses constant"
    },
    "Ø+": {
        "desc": """Signs; yield [1, -1].""",
        "keywords": "one minus negative constant"
    },
    "Ø-": {
        "desc": """Signs; yield [-1, 1].""",
        "keywords": "one minus negative constant"
    },
    "Ø.": {
        "desc": """Bits; yield [0, 1].""",
        "keywords": "one zero constant"
    },
    "Ø0": {
        "desc": """Zeroes; yield [0, 0].""",
        "keywords": "zero pair constant"
    },
    "Ø1": {
        "desc": """Ones; yield [1, 1].""",
        "keywords": "one pair constant"
    },
    "Ø2": {
        "desc": """Twos; yield [2, 2].""",
        "keywords": "two pair constant"
    },
    "Ø<": {
        "desc": """Angle brackets; yield '<>'.""",
        "keywords": "angle brackets constant"
    },
    "ØA": {
        "desc": """Alphabet; yield 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.""",
        "keywords": "alphabet uppercase case constant"
    },
    "ØB": {
        "desc": """Base digits; yield '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.""",
        "keywords": "base digits constant"
    },
    "ØḄ": {
        "desc": """Consonants; yield 'bcdfghjklmnpqrstvwxyz'.""",
        "keywords": "consonants lowercase case constant"
    },
    "ØḂ": {
        "desc": """Consonants; yield 'BCDFGHJKLMNPQRSTVWXYZ'.""",
        "keywords": "consonants uppercase case constant"
    },
    "ØC": {
        "desc": """Consonants; yield 'BCDFGHJKLMNPQRSTVWXYZbcdfghjklmnpqrstvwxyz'.""",
        "keywords": "consonants constant"
    },
    "ØD": {
        "desc": """Digits; yield '0123456789'.""",
        "keywords": "digits constant"
    },
    "ØH": {
        "desc": """Hexadecimal digits; yield '0123456789ABCDEF'.""",
        "keywords": "hexadecimal digits constant"
    },
    "ØJ": {
        "desc": """Yield Jelly's codepage.""",
        "keywords": "jelly codepage code page constant"
    },
    "ØP": {
        "desc": """Pi""",
        "keywords": "pi constant"
    },
    "ØQ": {
        "desc": """Qwerty; yield ['QWERTYUIOP', 'ASDFGHJKL', 'ZXCVBNM'].""",
        "keywords": "qwerty keyboard constant"
    },
    "ØV": {
        "desc": """Yield 'ṘV' (a cheat for writing a quine.)""",
        "keywords": "quine cheat constant"
    },
    "ØW": {
        "desc": """Word; yield 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_'""",
        "keywords": "word characters constant"
    },
    "ØY": {
        "desc": """Consonants; yield 'BCDFGHJKLMNPQRSTVWXZbcdfghjklmnpqrstvwxz'.""",
        "keywords": "consonants constant"
    },
    "Ø[": {
        "desc": """Square brackets; yield '[]'.""",
        "keywords": "square brackets constant"
    },
    "Ø^": {
        "desc": """Slashes; yield '/\\'""",
        "keywords": "slashes constant"
    },
    "Øa": {
        "desc": """Alphabet; yield 'abcdefghijklmnopqrstuvwxyz'.""",
        "keywords": "alphabet lowercase case constant"
    },
    "Øb": {
        "desc": """Base64 digits; yield 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'""",
        "keywords": "base64 digits constant"
    },
    "Øc": {
        "desc": """Vowels; yield 'AEIOUaeiou'.""",
        "keywords": "vowels constant"
    },
    "Øe": {
        "desc": """Euler's number""",
        "keywords": "euler number e constant"
    },
    "Øh": {
        "desc": """Hexadecimal digits; yield '0123456789abcdef'.""",
        "keywords": "hexadecimal digits constant"
    },
    "Øp": {
        "desc": """Phi (golden ratio)""",
        "keywords": "phi golden ratio constant"
    },
    "Øq": {
        "desc": """Qwerty; yield ['qwertyuiop', 'asdfghjkl', 'zxcvbnm'].""",
        "keywords": "qwerty keyboard constant"
    },
    "Øv": {
        "desc": """Yield 'Ṙv'.""",
        "keywords": "quine cheat constant"
    },
    "Øy": {
        "desc": """Vowels; yield 'AEIOUYaeiouy'.""",
        "keywords": "vowels constant"
    },
    "Ø{": {
        "desc": """Curly brackets; yield '{}'.""",
        "keywords": "curly brackets constant"
    },
    "Ø°": {
        "desc": """Yield 360.""",
        "keywords": "360 constant"
    },
    "Ø⁵": {
        "desc": """Yield 250 (Jelly's integer compression base).""",
        "keywords": "250 constant"
    },
    "Ø⁷": {
        "desc": """Yield 128 (2<sup>7</sup>).""",
        "keywords": "128 constant"
    },
    "ØẠ": {
        "desc": """Alphabet; yield 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.""",
        "keywords": "alphabet constant"
    },
    "ØỴ": {
        "desc": """Consonants; yield 'bcdfghjklmnpqrstvwxz'.""",
        "keywords": "consonants lowercase case constant"
    },
    "Øẹ": {
        "desc": """Vowels; yield 'aeiou'.""",
        "keywords": "vowels lowercase case constant"
    },
    "Øỵ": {
        "desc": """Vowels; yield 'aeiouy'.""",
        "keywords": "vowels lowercase case constant"
    },
    "ØṖ": {
        "desc": """Yield the set of printable ASCII chars.""",
        "keywords": "printable ascii constant"
    },
    "ØẎ": {
        "desc": """Consonants; yield 'BCDFGHJKLMNPQRSTVWXZ'.""",
        "keywords": "consonants uppercase case constant"
    },
    "Øė": {
        "desc": """Vowels; yield 'AEIOU'.""",
        "keywords": "vowels uppercase case constant"
    },
    "Øẏ": {
        "desc": """Vowels; yield 'AEIOUY'.""",
        "keywords": "vowels uppercase case constant"
    },
    "Æ!": {
        "desc": """Convert from integer to factorial base.""",
        "keywords": "convert integer factorial base"
    },
    "Æ¡": {
        "desc": """Convert from factorial base to integer.""",
        "keywords": "convert integer factorial base"
    },
    "Æ?": {
        "desc": """Convert from integer to primorial base.""",
        "keywords": "convert integer primorial base"
    },
    "Æ¿": {
        "desc": """Convert from primorial base to integer.""",
        "keywords": "convert integer primorial base"
    },
    "Æ½": {
        "desc": """Compute the integer square root of z.""",
        "keywords": "integer square root"
    },
    "ÆA": {
        "desc": """Arccosine.""",
        "keywords": "arccosine cosine acos"
    },
    "ÆC": {
        "desc": """Count the primes less or equal to z.""",
        "keywords": "count primes"
    },
    "ÆD": {
        "desc": """Compute the array of z's divisors.""",
        "keywords": "divisors divisible"
    },
    "ÆE": {
        "desc": """Compute the array of exponents of z's prime factorization. Includes zero exponents.""",
        "keywords": "exponents powers primes divisors"
    },
    "ÆF": {
        "desc": """Compute z's prime factorization as [prime, exponent] pairs.""",
        "keywords": "exponents powers primes divisors"
    },
    "ÆN": {
        "desc": """Generate the z<sup>th</sup> prime.""",
        "keywords": "primes indices"
    },
    "ÆP": {
        "desc": """If z is a prime, then 1, else 0 (has the shorter alias Ẓ).""",
        "keywords": "primes"
    },
    "ÆR": {
        "desc": """Range; generate all primes between 2 and z.""",
        "keywords": "primes range"
    },
    "ÆS": {
        "desc": """Sine.""",
        "keywords": "sine"
    },
    "ÆT": {
        "desc": """Tangent.""",
        "keywords": "tangent"
    },
    "Æc": {
        "desc": """Carmichael function.""",
        "keywords": "carmichael function"
    },
    "Æd": {
        "desc": """Divisor count.""",
        "keywords": "divisors divisible count"
    },
    "Æe": {
        "desc": """Exponential function.""",
        "keywords": "exponential function euler number e"
    },
    "Æf": {
        "desc": """Compute the array of primes whose product is z.""",
        "keywords": "primes"
    },
    "Æi": {
        "desc": """Separate a number z into [real(z), imag(z)].""",
        "keywords": "real imaginary pair complex"
    },
    "Æị": {
        "desc": """Combines the first two elements of a list into a complex number, z[0] + 1j*z[1]. Missing values are replaced with zeroes and extra values are truncated.""",
        "keywords": "real imaginary complex"
    },
    "Æl": {
        "desc": """Natural logarithm.""",
        "keywords": "natural logarithm euler number e"
    },
    "Æm": {
        "desc": """Arithmetic mean.""",
        "keywords": "arithmetic mean"
    },
    "Æn": {
        "desc": """Next; generate the closest prime strictly greater than z.""",
        "keywords": "next primes"
    },
    "Æp": {
        "desc": """Previous; generate the closest prime strictly less than z.""",
        "keywords": "previous primes"
    },
    "Ær": {
        "desc": """Find the roots of a polynomial, given a list of coefficients.""",
        "keywords": "solve solutions polynomial function roots"
    },
    "Æs": {
        "desc": """Divisor sum.""",
        "keywords": "divisors divisible sum"
    },
    "Æv": {
        "desc": """Count distinct prime factors.""",
        "keywords": "primes divisors divisible count"
    },
    "ÆẠ": {
        "desc": """Cosine.""",
        "keywords": "cosine"
    },
    "ÆĊ": {
        "desc": """Returns the zth Catalan number""",
        "keywords": "catalan numbers"
    },
    "ÆḌ": {
        "desc": """Proper divisors.""",
        "keywords": "divisors divisible"
    },
    "ÆẸ": {
        "desc": """Inverse of ÆE.""",
        "keywords": "exponents powers primes divisors"
    },
    "ÆṢ": {
        "desc": """Arcsine.""",
        "keywords": "arcsine asin sine"
    },
    "ÆṬ": {
        "desc": """Arctangent.""",
        "keywords": "arctangent atan tangent"
    },
    "ÆḊ": {
        "desc": """Determinant. For non-square z, computes det(zz<sup>T</sup>)<sup>½</sup>; if z is a row vector, this is its norm.""",
        "keywords": "matrix determinant norm"
    },
    "ÆḞ": {
        "desc": """Returns the zth item in the Fibonacci sequence.""",
        "keywords": "fibonacci indices"
    },
    "ÆĿ": {
        "desc": """Returns the zth Lucas number""",
        "keywords": "lucas numbers"
    },
    "ÆṪ": {
        "desc": """Totient function.""",
        "keywords": "euler totient function"
    },
    "Æḍ": {
        "desc": """Proper divisor count.""",
        "keywords": "divisors divisible count"
    },
    "Æṃ": {
        "desc": """Mode. Vectorizes.""",
        "keywords": "mode"
    },
    "Æṛ": {
        "desc": """Construct the polynomial with roots z. Returns list of coefficients.""",
        "keywords": "polynomial coefficients roots"
    },
    "Æṣ": {
        "desc": """Proper divisor sum.""",
        "keywords": "divisors divisible sum"
    },
    "Æṭ": {
        "desc": """Trace.""",
        "keywords": "trace"
    },
    "Æṁ": {
        "desc": """Median. Vectorizes.""",
        "keywords": "median"
    },
    "Æ°": {
        "desc": """Convert z from radians to degrees.""",
        "keywords": "convert radians degrees"
    },
    "Æ²": {
        "desc": """If z is a square, then 1, else 0.""",
        "keywords": "square second power two"
    },
    "æ.": {
        "desc": """Dot product of two vectors (real/complex). Right pads the shorter argument with 1.""",
        "keywords": "dot product vectors"
    },
    "æ×": {
        "desc": """Matrix multiplication.""",
        "keywords": "matrix multiplication"
    },
    "æ%": {
        "desc": """Symmetric modulo 2y; map x in the interval (−y, y]. Try 100Ræ%4 to get the hang of it.""",
        "keywords": "symmetric modulo"
    },
    "æ*": {
        "desc": """Matrix power.""",
        "keywords": "matrix power exponentiation"
    },
    "æA": {
        "desc": """Arctangent with two arguments, i.e., atan2().""",
        "keywords": "arctangent2 tangent atan2 two"
    },
    "æC": {
        "desc": """Convolution power.""",
        "keywords": "convolution power"
    },
    "æR": {
        "desc": """Inclusive prime range, from x to y.""",
        "keywords": "inclusive primes range"
    },
    "æc": {
        "desc": """Convolution of x and y.""",
        "keywords": "convolution"
    },
    "æi": {
        "desc": """Modular inverse of x, modulo y, or if none exists, 0.""",
        "keywords": "modular inverse modulo"
    },
    "æị": {
        "desc": """Combines x and y into a complex number as x + 1j*y.""",
        "keywords": "real imaginary complex"
    },
    "æl": {
        "desc": """Lowest common multiple (LCM).""",
        "keywords": "lowest smallest common multiple divisible"
    },
    "ær": {
        "desc": """Round x to the nearest multiple of 10<sup>−y</sup>.""",
        "keywords": "round power ten digits"
    },
    "æp": {
        "desc": """Precision; round x to y significant figures.""",
        "keywords": "precision round significant digits"
    },
    "æċ": {
        "desc": """Ceil x to the nearest power of y.""",
        "keywords": "ceiling round up power"
    },
    "æḟ": {
        "desc": """Floor x to the nearest power of y.""",
        "keywords": "floor round down power"
    },
    "æ«": {
        "desc": """Bit shift; compute x × 2<sup>y</sup>.""",
        "keywords": "bitshift shift bitwise"
    },
    "æ»": {
        "desc": """Bit shift; compute x × 2<sup>−y</sup>. Returns an integer.""",
        "keywords": "bitshift shift bitwise"
    },
    "Œ!": {
        "desc": """All permutations of z. May contain duplicates.""",
        "keywords": "permutations"
    },
    "Œ¿": {
        "desc": """Index of permutation z in a lexicographically sorted list of all permutations of z's items.""",
        "keywords": "permutations index"
    },
    "Œ?": {
        "desc": """Shortest permutation of items [1,2,...,N] which would yield z via Œ¿.""",
        "keywords": "permutations index"
    },
    "ŒB": {
        "desc": """Bounce; yield z[:-1] + z[::-1]. Vectorizes at depth 1.""",
        "keywords": "bounce palindrome"
    },
    "ŒḄ": {
        "desc": """Bounce; yield z[:-1] + z[::-1]. Does not vectorize.""",
        "keywords": "bounce palindrome"
    },
    "ŒḂ": {
        "desc": """Check if z is a palindrome. For integers, short for DŒḂ$.""",
        "keywords": "palindrome"
    },
    "ŒD": {
        "desc": """Diagonals of a matrix. Starts with the main diagonal.""",
        "keywords": "matrix diagonals"
    },
    "ŒĖ": {
        "desc": """Multidimensional enumerate.""",
        "keywords": "enumerate indices pairs multidimensional dimensions"
    },
    "ŒG": {
        "desc": """GET request z. http:// is prepended by default.""",
        "keywords": "http url get request"
    },
    "ŒĠ": {
        "desc": """Group multidimensional indices by their corresponding values.""",
        "keywords": "group indices values multidimensional dimensions"
    },
    "ŒH": {
        "desc": """Split z into two halves with similar length.""",
        "keywords": "halve half divide two lists arrays"
    },
    "ŒJ": {
        "desc": """Multidimensional indices of z.""",
        "keywords": "lists arrays indices range multidimensional dimensions"
    },
    "Œb": {
        "desc": """Like ŒṖ, but returns [[]] for the empty list.""",
        "keywords": "partitions"
    },
    "Œc": {
        "desc": """Unordered pairs (œc2).""",
        "keywords": "unordered combinations two pairs"
    },
    "Œċ": {
        "desc": """Unordered pairs with replacement (œċ2).""",
        "keywords": "unordered combinations two pairs replacement"
    },
    "Œd": {
        "desc": """Antidiagonals of a matrix. Starts with the main antidiagonal.""",
        "keywords": "matrix antidiagonals"
    },
    "Œḍ": {
        "desc": """Reconstruct matrix from its antidiagonals.""",
        "keywords": "matrix antidiagonals"
    },
    "Œg": {
        "desc": """Group runs of equal elements. Vectorizes at depth 1.""",
        "keywords": "group equals"
    },
    "Œɠ": {
        "desc": """Group run lengths. Does not vectorize.""",
        "keywords": "group runs rle run-length runlength"
    },
    "Œl": {
        "desc": """Lower case.""",
        "keywords": "lowercase case"
    },
    "ŒM": {
        "desc": """Return all multidimensional indices of z that correspond to maximal elements.""",
        "keywords": "maximal indices multidimensional dimensions"
    },
    "Œœ": {
        "desc": """Odd-even. Same as s2Z. [1, 2, 3, 4] gives [[1, 3], [2, 4]].""",
        "keywords": "odd even interleave"
    },
    "Œr": {
        "desc": """Run-length encode. “aab”Œr is [['a', 2], ['b', 1]].""",
        "keywords": "rle run-length runlength encode"
    },
    "Œṙ": {
        "desc": """Run-length decode. Right inverse of Œr.""",
        "keywords": "rle run-length runlength decode"
    },
    "Œs": {
        "desc": """Swap case.""",
        "keywords": "case swapcase"
    },
    "Œt": {
        "desc": """Title case.""",
        "keywords": "case titlecase"
    },
    "ŒỤ": {
        "desc": """Grade the multidimensional array z up, i.e., sort its multidimensional indices by their corresponding values.""",
        "keywords": "grade up sort indices multidimensional dimensions"
    },
    "Œu": {
        "desc": """Upper case.""",
        "keywords": "case uppercase"
    },
    "ŒP": {
        "desc": """Powerset of z. May contain duplicates.""",
        "keywords": "powerset subsets subsequences"
    },
    "Œp": {
        "desc": """Cartesian product of z's items.""",
        "keywords": "cartesian product"
    },
    "ŒṖ": {
        "desc": """Partition of z (z must be a list).""",
        "keywords": "partitions"
    },
    "Œṗ": {
        "desc": """Integer partitions of z (ways to sum positive integers to z)""",
        "keywords": "integer partitions"
    },
    "ŒḌ": {
        "desc": """Reconstruct matrix from its diagonals.""",
        "keywords": "matrix diagonals"
    },
    "ŒḊ": {
        "desc": """Depth.""",
        "keywords": "depth"
    },
    "ŒQ": {
        "desc": """Distinct sieve. (Replace each first occurrence of a value with 1, and all later occurrences with 0.)""",
        "keywords": "unique distinct sieve one zero"
    },
    "ŒR": {
        "desc": """List from −abs(z) to abs(z) inclusive (shorthand for Ar@N$).""",
        "keywords": "inclusive symmetric range"
    },
    "ŒṘ": {
        "desc": """Python's string representation.""",
        "keywords": "python code unevaluate representation"
    },
    "ŒT": {
        "desc": """Format time: Let the last three bits of z be abc. If a is 1, include the time; if b is 1, include the minute; if c is 1, include the second.""",
        "keywords": "format time"
    },
    "ŒṪ": {
        "desc": """Return all multidimensional indices of z that correspond to truthy elements, with depth 0.""",
        "keywords": "truthy indices multidimensional dimensions"
    },
    "ŒṬ": {
        "desc": """Return a multidimensional rectangular boolean array with 1s at the indices in z.""",
        "keywords": "truthy true boolean indices multidimensional dimensions"
    },
    "ŒV": {
        "desc": """Evaluate Python code z.""",
        "keywords": "evaluate python code"
    },
    "œ!": {
        "desc": """Permutations without replacement.""",
        "keywords": "permutations"
    },
    "œ&": {
        "desc": """Multiset intersection.""",
        "keywords": "multiset intersection xor"
    },
    "œ-": {
        "desc": """Multiset difference.""",
        "keywords": "multiset difference subtraction"
    },
    "œ^": {
        "desc": """Multiset symmetric difference.""",
        "keywords": "multiset difference symmetric"
    },
    "œ¿": {
        "desc": """Index of permutation x in a list of all permutations of x's items sorted by their index in y.""",
        "keywords": "permutations index"
    },
    "œ?": {
        "desc": """Permutation at index x of the items in y (where y defines the sort order of those items).""",
        "keywords": "permutations index"
    },
    "œc": {
        "desc": """Combinations without replacement.""",
        "keywords": "combinations"
    },
    "œẹ": {
        "desc": """Return the multidimensional indices of all occurrences of y in x.""",
        "keywords": "find indices multidimensional dimensions"
    },
    "œi": {
        "desc": """Find the first multidimensional index of element y in x. Returns empty list if there are no occurrences.""",
        "keywords": "find first indices multidimensional dimensions"
    },
    "œị": {
        "desc": """Element of y at multi-dimensional index x. If x is empty, return y. Otherwise, y is replaced by its element at index x[0] (behaves like ị), x is replaced by x[1:], and the procedure is repeated.""",
        "keywords": "indices multidimensional dimensions"
    },
    "œl": {
        "desc": """Trim all elements of y from the left side of x.""",
        "keywords": "trim left sides"
    },
    "œr": {
        "desc": """Trim all elements of y from the right side of x.""",
        "keywords": "trim right sides"
    },
    "œs": {
        "desc": """Split x into y chunks of similar lengths.""",
        "keywords": "split slices sublists length"
    },
    "œ|": {
        "desc": """Multiset union.""",
        "keywords": "multiset union addition"
    },
    "œS": {
        "desc": """After sleeping for y seconds, return x.""",
        "keywords": "sleep time"
    },
    "œṣ": {
        "desc": """Split x around sublists equal to y.""",
        "keywords": "split slices sublists"
    },
    "œċ": {
        "desc": """Combinations with replacement.""",
        "keywords": "combinations replacement"
    },
    "œṖ": {
        "desc": """Partition y at the indices in x.""",
        "keywords": "split partition truthy true indices"
    },
    "œṗ": {
        "desc": """Partition y before truthy indices of x.""",
        "keywords": "split partition truthy true indices"
    },
    "œP": {
        "desc": """Partition y at the indices in x, without keeping the borders.""",
        "keywords": "split partition truthy true indices"
    },
    "œp": {
        "desc": """Partition y at truthy indices of x, without keeping the borders.""",
        "keywords": "split partition truthy true indices"
    },
    "œṡ": {
        "desc": """Split x at the first occurrence of y.""",
        "keywords": "split first separator"
    },
    "Œe": {
        "desc": """Return the items at even positions in z.""",
        "keywords": "filter odd even"
    },
    "Œo": {
        "desc": """Return the items at odd positions in z.""",
        "keywords": "filter odd even"
    },
}

QUICKS = {
    "©": {
        "desc": """Copy link result to register (® atom to retrieve).""",
        "keywords": "copy store register"
    },
    "ß": {
        "desc": """This link, with the same arity.""",
        "keywords": "same link arity"
    },
    "¢": {
        "desc": """Last link as a nilad.""",
        "keywords": "last previous link nilad"
    },
    "Ç": {
        "desc": """Last link as a monad.""",
        "keywords": "last previous link monad"
    },
    "ç": {
        "desc": """Last link as a dyad.""",
        "keywords": "last previous link dyad"
    },
    "Ñ": {
        "desc": """Next link as a monad.""",
        "keywords": "next link monad"
    },
    "ñ": {
        "desc": """Next link as a dyad.""",
        "keywords": "next link dyad"
    },
    "£": {
        "desc": """Link at index n as a nilad.""",
        "keywords": "link indices nilad"
    },
    "Ŀ": {
        "desc": """Link at index n as a monad.""",
        "keywords": "link indices monad"
    },
    "ŀ": {
        "desc": """Link at index n as a dyad.""",
        "keywords": "link indices dyad"
    },
    "¦": {
        "desc": """Apply link to items at specific indices.""",
        "keywords": "apply link indices"
    },
    "¡": {
        "desc": """Repeat n times.""",
        "keywords": "repeat loop link"
    },
    "¿": {
        "desc": """While loop.""",
        "keywords": "while loop link"
    },
    "/": {
        "desc": """Reduce or n-wise reduce.""",
        "keywords": "reduce link"
    },
    "ƒ": {
        "desc": """Reduce or n-wise reduce using the right argument as the starting value.""",
        "keywords": "reduce link starting"
    },
    "\\": {
        "desc": """Cumulative reduce or n-wise overlapping reduce.""",
        "keywords": "cumulative reduce overlapping link"
    },
    "¤": {
        "desc": """Nilad followed by links as a nilad.""",
        "keywords": "links nilad"
    },
    "$": {
        "desc": """Last two links (if not part of an LCC) as a monad""",
        "keywords": "two links monad"
    },
    "Ɗ": {
        "desc": """Last three links (if not part of an LCC) as a monad""",
        "keywords": "three links monad"
    },
    "Ʋ": {
        "desc": """Last four links (if not part of an LCC) as a monad""",
        "keywords": "four links monad"
    },
    "¥": {
        "desc": """Last two links (if not part of an LCC) as a dyad""",
        "keywords": "two links dyad"
    },
    "ɗ": {
        "desc": """Last three links (if not part of an LCC) as a dyad""",
        "keywords": "three links dyad"
    },
    "ʋ": {
        "desc": """Last four links (if not part of an LCC) as a dyad""",
        "keywords": "four links dyad"
    },
    "#": {
        "desc": """nfind: Count up, collecting first n matches.""",
        "keywords": "find count link"
    },
    "?": {
        "desc": """Ternary if.""",
        "keywords": "ternary if conditional"
    },
    "Ƒ": {
        "desc": """Check if the left argument is equal to the result.""",
        "keywords": "compare equals equality result changed link"
    },
    "⁺": {
        "desc": """Duplicates the previous link.""",
        "keywords": "duplicate double link"
    },
    "@": {
        "desc": """Swaps operands.""",
        "keywords": "swap arguments link"
    },
    "`": {
        "desc": """Make a monad from a dyad by repeating the argument.""",
        "keywords": "monad dyad repeat argument link"
    },
    "\"": {
        "desc": """Vectorize/zipwith.""",
        "keywords": "vectorize zipwith link"
    },
    "'": {
        "desc": """For monads, flat. For dyad, spawn.""",
        "keywords": "flat spawn link"
    },
    "þ": {
        "desc": """Outer product/table.""",
        "keywords": "outer product table link"
    },
    "{": {
        "desc": """Turn a monad into a dyad. Uses the left argument.""",
        "keywords": "monad dyad left argument link"
    },
    "}": {
        "desc": """Turn a monad into a dyad. Uses the right argument.""",
        "keywords": "monad dyad right argument link"
    },
    "€": {
        "desc": """Each. Map a link over its left argument.""",
        "keywords": "each map link left argument"
    },
    "Þ": {
        "desc": """Sort by some key function.""",
        "keywords": "sorted key link"
    },
    "Ɲ": {
        "desc": """Apply a dyadic link or a monadic chain for all pairs of neighboring elements.""",
        "keywords": "adjacent pairs link"
    },
    "Ƥ": {
        "desc": """Map a link over prefixes, and if given a <nilad>, overlapping infixes if positive, or the overlapping outfixes of abs(<nilad>) if negative.""",
        "keywords": "prefixes link"
    },
    "ƙ": {
        "desc": """Key. Map a link over the groups formed by identical items.""",
        "keywords": "group equals equality link"
    },
    "ɼ": {
        "desc": """Register. Apply link to ® then copy to the register and return the result.""",
        "keywords": "register store load copy modify"
    },
    "ƭ": {
        "desc": """Tie. Cycle through a number of (default 2) links each time called.""",
        "keywords": "tie cycle links"
    },
    "Ɱ": {
        "desc": """Each (alias for Ð€). Map a link over its right argument.""",
        "keywords": "each map link right alternate argument"
    },
    "Ð¡": {
        "desc": """Like ¡. Collects all intermediate results.""",
        "keywords": "repeat loop link cumulative collect"
    },
    "Ð¿": {
        "desc": """Like ¿. Collects all intermediate results.""",
        "keywords": "while loop link cumulative collect"
    },
    "ÐƤ": {
        "desc": """Map a link over suffixes, and if given a <nilad>, non-overlapping infixes if positive, or the non-overlapping outfixes of abs(<nilad>) if negative.""",
        "keywords": "suffixes link"
    },
    "Ƈ": {
        "desc": """Filter (alias for Ðf). Keep all items that satisfy a condition.""",
        "keywords": "filter link keep conditional"
    },
    "Ðḟ": {
        "desc": """Filter. Discard all items that satisfy a condition.""",
        "keywords": "filter inverted link remove conditional"
    },
    "ÐL": {
        "desc": """Loop. Repeat until the results are no longer unique.""",
        "keywords": "loop link unique"
    },
    "Ƭ": {
        "desc": """Like ÐL (alias for ÐĿ). Collects all intermediate results.""",
        "keywords": "loop link unique cumulative collect"
    },
    "ÐḶ": {
        "desc": """Like ÐL. Collects all results in the loop.""",
        "keywords": "loop link unique cumulative collect"
    },
    "ÐṂ": {
        "desc": """Keep elements with minimal link value; [e for e in z if link(e) == min(map(link, z))].""",
        "keywords": "link minimum"
    },
    "ÐṀ": {
        "desc": """Keep elements with maximal link value; [e for e in z if link(e) == max(map(link, z))].""",
        "keywords": "link maximum"
    },
    "Ðe": {
        "desc": """Apply link to even indices.""",
        "keywords": "apply link even indices"
    },
    "Ðo": {
        "desc": """Apply link to odd indices.""",
        "keywords": "apply link odd indices"
    },
    "Ð€": {
        "desc": """Each. Map a link over its right argument.
Usage: <dyad>Ð€""",
        "keywords": "each map link right alternate argument"
    },
    "Ðf": {
        "desc": """Filter. Keep all items that satisfy a condition.
Usage: <condition>Ðf""",
        "keywords": "filter link keep conditional"
    },
    "ÐĿ": {
        "desc": """Like ÐL. Collects all intermediate results.
Usage: <link>ÐĿ""",
        "keywords": "loop link unique cumulative"
    },
}

SYNTAX = {
    "⁾": {
        "desc": """Begins a 2-char string literal.""",
        "keywords": "string character literal two delimiter beginning"
    },
    "⁽": {
        "desc": """Begins a 2-digit base-250 number. If the number is larger than 31500, subtracts 62850, otherwise adds 750.""",
        "keywords": "number literal base 250 two digits delimiter beginning"
    },
    "ø": {
        "desc": """Starts a separate niladic chain.""",
        "keywords": "new niladic chain"
    },
    "µ": {
        "desc": """Starts a separate monadic chain.""",
        "keywords": "new monadic chain"
    },
    ")": {
        "desc": """Equivalent to µ€. Maps the chain to the left over its left argument. Unlike µ, the chain following ) retains the same left argument as the chain popped by ).""",
        "keywords": "new monadic chain map each"
    },
    "ð": {
        "desc": """Starts a separate dyadic chain.""",
        "keywords": "new dyadic chain"
    },
    "ɓ": {
        "desc": """Starts a separate dyadic chain with reversed arguments.""",
        "keywords": "new dyadic chain reversed arguments"
    },
    "ı": {
        "desc": """Complex number. Without arguments, 1j.""",
        "keywords": "complex imaginary number literal"
    },
    "ȷ": {
        "desc": """Decimal number, as in 2e6 for 2000000. Without arguments, 1000.""",
        "keywords": "decimal exponential number literal"
    },
    "-": {
        "desc": """Negative number. Without arguments, -1.""",
        "keywords": "negative number literal"
    },
    ".": {
        "desc": """Decimal number. Without arguments, 0.5.""",
        "keywords": "decimal number comma literal"
    },
    ",": {
        "desc": """Separate elements of a list. If any of the arguments is not a literal, or spaces separate the arguments, , will be treated as the "pair" atom instead.""",
        "keywords": "list literal delimiter separator"
    },
    "[": {
        "desc": """Begins a comma-separated list. Note that [] doesn't work as the empty list, use “” or ⁸ instead.""",
        "keywords": "list literal delimiter beginning"
    },
    "]": {
        "desc": """Ends a comma-separated list.""",
        "keywords": "list literal delimiter ending"
    },
    "“”": {
        "desc": """A regular string literal.""",
        "keywords": "string literal"
    },
    "“»": {
        "desc": """A dictionary-compressed string literal.""",
        "keywords": "string literal compressed"
    },
    "“‘": {
        "desc": """A code-page index list. Jelly's version of ord().""",
        "keywords": "number list literal ordinal"
    },
    "“’": {
        "desc": """A base-250 number.""",
        "keywords": "number literal base 250"
    },
}

ALL = {**ATOMS, **QUICKS, **SYNTAX}
