import codecs
import jelly

JELLY_DECODE = jelly.code_page
JELLY_ENCODE = codecs.charmap_build(JELLY_DECODE)


def encode(data, errors="strict"):
    """Encodes a string to the Jelly codepage."""
    data = data.replace("\n", "¶")
    return codecs.charmap_encode(data, errors, JELLY_ENCODE)


def decode(data, errors="strict"):
    """Decodes a string from the Jelly codepage, performing minimal parsing
    for string literals to replace link-separating pilcrows with newlines."""
    data, length = codecs.charmap_decode(data, errors, JELLY_DECODE)
    data_out = ""
    skip = 0
    string = False
    for char in data:
        if string:
            if char == "”" or char == "»" or char == "‘" or char == "’":
                string = False
        elif skip:
            skip -= 1
        elif char == "⁾" or char == "⁽":
            skip = 2
        elif char == "”":
            skip = 1
        elif char == "“":
            string = True
        elif char == "¶":
            char = "\n"
        data_out += char
    return data_out, length


def jelly_codec_search(name):
    if name == "jelly":
        return codecs.CodecInfo(name="jelly", encode=encode, decode=decode)
    return None


def register():
    """Registers the Jelly codec."""
    codecs.register(jelly_codec_search)
