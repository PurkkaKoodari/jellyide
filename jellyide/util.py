def safe_repr(obj):
    try:
        length = len(obj)
        if length >= 50:
            try:
                ret = repr(obj[:50])
            except Exception:
                ret = "<" + str(length) + "-item " + type(obj).__name__ + ">"
        else:
            ret = repr(obj)
    except Exception:
        ret = repr(obj)
    if len(ret) >= 300:
        return ret[:300] + "..."
    return ret
