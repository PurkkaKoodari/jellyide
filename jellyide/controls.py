import tkinter


class Menu(tkinter.Menu):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, tearoff=False, **kwargs)

    def add_option(self, label, command=None, accelerator=None, **kwargs):
        conf = {
            "label": label,
            "command": command
        }
        conf.update(kwargs)
        evt = None
        if accelerator is not None:
            accelerator = accelerator.split("-")
            accel = accelerator[-1].title()
            evt = accelerator[-1]
            if "Shift" in accelerator:
                accel = "Shift+" + accel
                evt = "Shift-" + evt
            if "Alt" in accelerator:
                accel = "Alt+" + accel
                evt = "Alt-" + evt
            if "Ctrl" in accelerator:
                accel = "Ctrl+" + accel
                evt = "Control-" + evt
            if len(evt) > 1:
                evt = "<" + evt + ">"
            conf["accelerator"] = accel
        self.add_command(conf)
        if evt is not None and command is not None:
            def handler(_):
                command()
            self.bind_all(evt, handler)


class TextWithVar(tkinter.Text):
    """A text widget that accepts a 'textvariable' option.

    Adapted from https://stackoverflow.com/a/21565476/1938435
    """
    def __init__(self, parent, *args, textvariable=None, **kwargs):
        self._textvariable = textvariable
        tkinter.Text.__init__(self, parent, *args, **kwargs)

        # if the variable has data in it, use it to initialize the widget
        if self._textvariable is not None:
            self.insert("1.0", self._textvariable.get())

        # define an internal proxy which generates a virtual event whenever text is inserted or deleted
        self.tk.eval("""
        proc widget_proxy {widget widget_command args} {
            # call the real tk widget command with the real args
            set result [uplevel [linsert $args 0 $widget_command]]

            # if the contents changed, generate an event we can bind to
            if {([lindex $args 0] in {insert replace delete})} {
                event generate $widget <<Change>> -when tail
            }
            # return the result from the real widget command
            return $result
        }
        """)

        # replace the underlying widget with the proxy
        self.tk.eval("""
        rename {widget} _{widget}
        interp alias {{}} ::{widget} {{}} widget_proxy {widget} _{widget}
        """.format(widget=str(self)))

        # set up a binding to update the variable whenever
        # the widget changes
        self.bind("<<Change>>", self._on_widget_change)

        # set up a trace to update the text widget when the
        # variable changes
        if self._textvariable is not None:
            self._textvariable.trace("wu", self._on_var_change)

    def _on_var_change(self, *args):
        """Change the text widget when the associated textvariable changes"""
        # only change the widget if something actually changed, otherwise we'll get into an endless loop
        text_current = self.get("1.0", "end-1c")
        var_current = self._textvariable.get()
        if text_current != var_current:
            self.delete("1.0", "end")
            self.insert("1.0", var_current)

    def _on_widget_change(self, event=None):
        """Change the variable when the widget changes"""
        if self._textvariable is not None:
            self._textvariable.set(self.get("1.0", "end-1c"))
