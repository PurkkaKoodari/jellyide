from jellyide.data import ALL

SYNONYMS = [
    ["addition", "plus"],
    ["subtraction", "substraction", "minus"],
    ["multiply", "multiplication", "times"],
    ["division", "divided"],
    ["divisible", "divisibility"],
    ["divisors", "factors", "factorization"],
    ["modulo", "modulus"],
    ["split", "explode"],
    ["join", "implode"],
    ["argument", "parameter"],
    ["parentheses", "parenthesis", "brackets"],
    ["zero", "0"],
    ["one", "1"],
    ["two", "2"],
    ["three", "3"],
    ["four", "4"],
    ["ten", "10"],
    ["sixteen", "16"],
    ["hundred", "100"],
    ["linebreaks", "newlines", "linefeeds", "breaks"],
    ["first", "1st"],
    ["second", "2nd"],
    ["third", "3rd"],
    ["fourth", "4th"],
    ["fifth", "5th"],
    ["sixth", "6th"],
    ["seventh", "7th"],
    ["list", "array"],
    ["retrieve", "get"],
    ["store", "put", "save"],
    ["indices", "indexes"],
    ["rotate", "shift"],
    ["sublists", "substrings"],
    ["pairs", "tuples"],
    ["find", "search"],
    ["minimum", "minimal", "smallest"],
    ["maximum", "maximal", "largest"],
    ["euler's", "euler", "eulers"],
    ["trim", "strip"],
    ["not", "inverted", "inverse"],
    ["contains", "in", "contained"],
    ["same", "this"],
    ["next", "following"],
    ["previous", "preceding"],
    ["double", "twice"],
    ["cumulative", "collecting"],
    ["remove", "discard"],
    ["beginning", "starting"],
    ["ending", "terminating", "terminator", "terminate"],
    ["number", "numeric"]
]

INSERTABLE = [
    {
        **details,
        "name": link,
        "add": (lambda link: lambda: link)(link),  # please forgive me
        "keywords": [
            next((words for words in SYNONYMS if keyword in words), [keyword])
            for keyword in details["keywords"].split()
        ],
    } for link, details in ALL.items()
]


def find_inserts(query):
    """Sorts the insertables by relevance."""
    query = query.strip()
    if query == "":
        return []
    words = query.split()
    for insert in INSERTABLE:
        linkmatchcase = any(insert["name"].startswith(word) for word in words)
        linkmatch = any(insert["name"].lower().startswith(word.lower()) for word in words)
        count = sum(
            any(synonym.startswith(word.lower())for synonyms in insert["keywords"] for synonym in synonyms)
            for word in words
        )
        keywordcount = sum(
            any(synonym.startswith(word.lower()) for word in words for synonym in synonyms)
            for synonyms in insert["keywords"]
        )
        insert["match"] = (linkmatchcase, linkmatch, count, keywordcount / len(insert["keywords"]))
    return sorted(filter(lambda insert: any(insert["match"]), INSERTABLE), key=lambda insert: insert["match"], reverse=True)
