import os

from PIL import Image
from PIL.ImageDraw import Draw
from PIL.ImageFont import truetype

# links colored by arity, with lighter/darker colors alternating when nested
from jelly import interpreter

ARITY_COLORS = {
    0: ((255, 247, 153), (245, 230, 61)),
    1: ((153, 255, 153), (61, 245, 61)),
    2: ((153, 153, 255), (61, 61, 245)),
    -1: ((255, 170, 153), (245, 92, 61)),
}

FONT_FILE = os.path.join(os.path.dirname(__file__), "/DejaVuSansMono.ttf")
FONT = truetype(FONT_FILE, 12)

XPADDING = 5
YPADDING = 8


def visualize_link(link, call_arity=None) -> Image.Image:
    if call_arity is not None and link.arity < 0:
        link.arity = call_arity

    color, border = ARITY_COLORS[link.arity]

    is_chain = hasattr(link, "children") and (hasattr(link, "chain") or link.name in "¤$ƊƲ¥ɗʋ")
    gaps = []
    if is_chain:
        assert link.arity >= 0 or link.arity == -1
        for child in link.children:
            if child.arity < 0:
                child.arity = link.arity
        chain = link.children[:]
        pos = 0
        while chain:
            if link.arity == 2:
                if interpreter.arities(chain[0:3]) == [2, 2, 0] and interpreter.leading_nilad(chain[2:]):
                    consume = 3
                elif interpreter.arities(chain[0:2]) in ([2, 2], [2, 0], [0, 2]):
                    consume = 2
                else:
                    consume = 1
            elif link.arity == 0 and chain[0].arity <= 0:
                consume = 1
                link.arity = 1
            else:
                if interpreter.arities(chain[0:2]) in ([2, 1], [2, 0], [0, 2]):
                    consume = 2
                else:
                    consume = 1
            pos += consume
            chain = chain[consume:]
            if chain:
                gaps.append(pos)

    children = link.get("children", [])
    childimg = [visualize_link(child) for child in children]
    childwidth = sum(child.width for child in childimg) + len(gaps) * XPADDING + XPADDING if childimg else 0

    if link.name is not None:
        tmpdraw = Draw(Image.new("RGBA", (1, 1)))
        namewidth, nameheight = tmpdraw.textsize(link.get("name", "?"), FONT)
        namewidth += XPADDING
    else:
        namewidth = 0
        nameheight = 0

    width = XPADDING + childwidth + namewidth
    height = YPADDING + max([child.height for child in childimg] + [nameheight]) + YPADDING
    image = Image.new("RGBA", (width, height), color)
    draw = Draw(image)
    draw.rectangle((0, 0, width - 1, height - 1), outline=border, width=2)

    x = XPADDING
    for i, child in enumerate(childimg):
        image.paste(child, (x, (height - child.height) // 2))
        x += child.width
        if i + 1 in gaps:
            x += XPADDING
    if childimg:
        x += XPADDING

    if link.name is not None:
        draw.text((x, (height - nameheight) // 2), link.name, (0, 0, 0), FONT)

    return image


def compute_link_arities(code):
    lines = interpreter.regex_flink.findall(code)
    refs = [set() for _ in lines]
    for index, line in enumerate(lines):
        for token in interpreter.regex_token.findall(line):
            if token == "¢":
                refs[index].add((index - 1, 0))
            elif token == "Ç":
                refs[index].add((index - 1, 1))
            elif token == "ç":
                refs[index].add((index - 1, 2))
            elif token == "Ñ":
                refs[index].add(((index + 1) % len(lines), 1))
            elif token == "ñ":
                refs[index].add(((index + 1) % len(lines), 2))

    prev_arities = set()
    used_arities = {(len(lines) - 1, 0)}
    while used_arities != prev_arities:
        prev_arities = set(used_arities)
        for src, _ in prev_arities:
            used_arities.update(refs[src])
    return sorted(used_arities)


def visualize_code(code: str):
    links = interpreter.parse_code(code)
    if not links:
        return None
    arities = compute_link_arities(code)
    # TODO: visualize arguments
    link_imgs = []
    for index, arity in arities:
        link_imgs.append(visualize_link(interpreter.create_chain(links[index]), arity))
    if not link_imgs:
        return None
    width = max(img.width for img in link_imgs)
    height = sum(img.height for img in link_imgs)
    combined_img = Image.new("RGBA", (width, height), (0, 0, 0, 0))
    y = 0
    for img in link_imgs:
        combined_img.paste(img, (0, y))
        y += img.height
    return combined_img
