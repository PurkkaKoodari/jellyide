import tkinter
import tkinter.font
import tkinter.messagebox
import tkinter.filedialog
import threading
import traceback

from PIL.ImageTk import PhotoImage

from jelly import interpreter

from jellyide import insert, controls, codec
from jellyide.debugger import DebugState, Debugger, StopRunning, JellyException, format_jelly_exception
from jellyide.util import safe_repr
from jellyide.visualizer import visualize_code

codec.register()

HELLO_WORLD = "“3ḅaė;œ»"
UNTITLED = "untitled.jelly"

FILE_FILTER = (("Jelly code files", "*.jelly"), ("All files", "*.*"))

VERSION = "0.0.2"


# noinspection PyAttributeOutsideInit
class JellyIde:
    def __init__(self, argv=()):
        self.ui = ui = tkinter.Tk(className="Jellyide")
        ui.grid_rowconfigure(1, weight=1)
        ui.grid_columnconfigure(0, weight=1)
        ui.protocol("WM_DELETE_WINDOW", self.close_editor)

        self.debugger = Debugger(self)
        self.debugger.hook()
        self.runner = None

        self.undo_log = []
        self.redo_log = []
        self.saved_state = 0
        self.keep_undo = False

        self.code = tkinter.StringVar()
        self.prev_code = ""

        self.filename = tkinter.StringVar()
        self.filename.set(UNTITLED)
        self.modified = tkinter.BooleanVar()
        self.modified.set(False)
        self.running = tkinter.BooleanVar()
        self.running.set(False)
        self.debug_state = tkinter.BooleanVar()
        self.debug_state.set(False)

        self.font = tkinter.font.Font(family="DejaVu Sans Mono", size=10)

        self.build_menus()
        self.build_toolbar()
        self.build_code()
        self.build_insert()
        self.build_console()

        self.insert_labels = []
        self.insert_desc_labels = []
        self.insert_entries = []
        self.insert_selected = tkinter.IntVar()
        self.insert_shown = False

        self.code_box.index(tkinter.END)
        self.update_visualization()

        self.filename.trace_add("write", lambda *args: self.update_title())
        self.modified.trace_add("write", lambda *args: self.update_title())
        self.code.trace_add("write", lambda *args: self.code_changed())
        self.update_title()
        self.running.trace_add("write", lambda *args: self.running_changed())
        self.debug_state.trace_add("write", lambda *args: self.running_changed())
        self.insert_text.trace_add("write", lambda *args: self.insert_changed())
        self.insert_selected.trace_add("write", lambda *args: self.insert_select_changed())

        self.saved = False

        if len(argv) > 1:
            self.ui.after(0, lambda: self.do_open(argv[1]))
        else:
            self.load_file(HELLO_WORLD)

    def build_menus(self):
        menubar = tkinter.Menu(self.ui)
        self.ui.config(menu=menubar)

        self.file_menu = file_menu = controls.Menu(menubar)
        file_menu.add_option("New", self.new_file, "Ctrl-n")
        file_menu.add_option("Open...", self.open_file, "Ctrl-o")
        file_menu.add_option("Save", self.save_file, "Ctrl-s")
        file_menu.add_option("Save As...", self.save_file_as, "Ctrl-Shift-s")
        file_menu.add_separator()

        self.encoding = tkinter.StringVar()
        self.encoding.set("auto")
        self.encoding.trace_add("write", lambda *args: self.encoding_changed())
        encoding_menu = controls.Menu(file_menu)
        encoding_menu.add_radiobutton(label="Automatic (prefer Jelly)", variable=self.encoding, value="auto")
        encoding_menu.add_radiobutton(label="Jelly", variable=self.encoding, value="jelly")
        encoding_menu.add_radiobutton(label="UTF-8", variable=self.encoding, value="utf-8")

        file_menu.add_cascade(label="Encoding", menu=encoding_menu)
        file_menu.add_separator()
        file_menu.add_option("Quit", self.close_editor, "Ctrl-q")
        menubar.add_cascade(label="File", menu=file_menu)

        self.edit_menu = edit_menu = controls.Menu(menubar)
        edit_menu.add_option("Undo", self.undo, "Ctrl-z", state=tkinter.DISABLED)
        edit_menu.add_option("Redo", self.redo, "Ctrl-y", state=tkinter.DISABLED)
        edit_menu.add_separator()
        edit_menu.add_option("Select All", self.select_all, "Ctrl-a")
        edit_menu.add_separator()
        edit_menu.add_option("Insert Code...", self.insert_code, "Ctrl-space")
        menubar.add_cascade(label="Edit", menu=edit_menu)

        self.run_menu = run_menu = controls.Menu(menubar)
        run_menu.add_option("Run", self.run_program, "Ctrl-F5")
        run_menu.add_option("Debug", self.debug_program, "F5")
        run_menu.add_separator()
        run_menu.add_option("Pause", self.pause_debug, "F8", state=tkinter.DISABLED)
        run_menu.add_option("Resume", self.resume_debug, "F9", state=tkinter.DISABLED)
        run_menu.add_option("Step", self.step_debug, "F10", state=tkinter.DISABLED)
        run_menu.add_option("Step to return", self.step_to_return, "Shift-F10", state=tkinter.DISABLED)
        run_menu.add_option("Stop", self.stop_program, "Shift-F5", state=tkinter.DISABLED)
        menubar.add_cascade(label="Run", menu=run_menu)

    def build_toolbar(self):
        self.toolbar = toolbar = tkinter.Frame(self.ui)
        toolbar.grid(row=0, column=0, sticky=tkinter.W + tkinter.E)

        self.run_btn = run_btn = tkinter.Button(toolbar, command=self.run_program, text="Run")
        run_btn.pack(side=tkinter.LEFT)

        self.debug_btn = debug_btn = tkinter.Button(toolbar, command=self.debug_program, text="Debug")
        debug_btn.pack(side=tkinter.LEFT)

        self.pause_btn = pause_btn = tkinter.Button(toolbar, command=self.pause_debug, text="Pause", state=tkinter.DISABLED)
        pause_btn.pack(side=tkinter.LEFT)

        self.resume_btn = resume_btn = tkinter.Button(toolbar, command=self.resume_debug, text="Resume", state=tkinter.DISABLED)
        resume_btn.pack(side=tkinter.LEFT)

        self.step_btn = step_btn = tkinter.Button(toolbar, command=self.step_debug, text="Step", state=tkinter.DISABLED)
        step_btn.pack(side=tkinter.LEFT)

        self.stop_btn = stop_btn = tkinter.Button(toolbar, command=self.stop_program, text="Stop", state=tkinter.DISABLED)
        stop_btn.pack(side=tkinter.LEFT)

    def build_code(self):
        self.code_area = code_area = tkinter.Frame(self.ui)
        code_area.grid(row=1, column=0, sticky=tkinter.N + tkinter.S + tkinter.W + tkinter.E)
        code_area.grid_rowconfigure(0, weight=1)
        code_area.grid_columnconfigure(0, weight=1)

        self.code_box = code_box = controls.TextWithVar(code_area, undo=False, font=self.font, textvariable=self.code)
        code_box.grid(row=0, column=0, sticky=tkinter.N + tkinter.S + tkinter.W + tkinter.E)
        code_box.focus_force()

        self.visualizer = visualizer = tkinter.Label(code_area)
        visualizer.grid(row=1, column=0, sticky=tkinter.N + tkinter.S + tkinter.W + tkinter.E)

        scrollbar = tkinter.Scrollbar(code_area, command=self.code_box.yview)
        scrollbar.grid(row=0, column=1, rowspan=2, sticky=tkinter.N + tkinter.S)
        code_box.configure(yscrollcommand=scrollbar.set)

    def build_insert(self):
        self.insert_area = insert_area = tkinter.Toplevel(self.ui)
        insert_area.title("JellyIDE Suggestions")
        insert_area.overrideredirect(True)
        insert_area.grid_columnconfigure(0, minsize=40)
        insert_area.grid_columnconfigure(1, weight=1)
        insert_area.withdraw()
        insert_area.bind("<FocusOut>", lambda _: self.hide_insert())

        self.insert_text = tkinter.StringVar()
        self.insert_box = insert_box = tkinter.Entry(insert_area, textvariable=self.insert_text)
        insert_box.grid(row=0, column=0, columnspan=2, sticky=tkinter.N + tkinter.S + tkinter.W + tkinter.E)
        insert_box.bind("<Escape>", lambda _: self.hide_insert())
        insert_box.bind("<Up>", lambda _: self.select_insert(-1))
        insert_box.bind("<Down>", lambda _: self.select_insert(1))
        insert_box.bind("<Return>", lambda _: self.do_insert())

    def build_console(self):
        self.console_window = console_window = tkinter.Toplevel(self.ui, class_="Jellyide")
        console_window.title("JellyIDE Console")
        console_window.grid_rowconfigure(0, weight=1)
        console_window.grid_columnconfigure(0, weight=1)
        console_window.protocol("WM_DELETE_WINDOW", self.close_console)
        console_window.withdraw()

        self.console = console = tkinter.Text(console_window, undo=False, state=tkinter.DISABLED, font=self.font)
        console.grid(row=0, column=0, sticky=tkinter.N + tkinter.S + tkinter.W + tkinter.E)

        scrollbar = tkinter.Scrollbar(console_window, command=self.console.yview)
        scrollbar.grid(row=0, column=1, sticky=tkinter.N + tkinter.S)
        console.configure(yscrollcommand=scrollbar.set)

    def close_console(self):
        self.debugger.stop()
        self.console.configure(state=tkinter.NORMAL)
        self.console.delete("1.0", tkinter.END)
        self.console.configure(state=tkinter.DISABLED)
        self.console_window.withdraw()

    def close_editor(self):
        if not self.save_unsaved("exiting"):
            return
        self.ui.quit()

    def do_save(self, filename):
        try:
            code = self.code.get()
            with open(filename, "wb") as stream:
                encoding = self.encoding.get()
                if encoding == "auto":
                    try:
                        stream.write(code.encode("jelly"))
                    except UnicodeError:
                        stream.write(code.encode("utf-8"))
                else:
                    stream.write(code.encode(encoding))
            self.saved = True
            self.modified.set(False)
            self.filename.set(filename)
            self.saved_state = len(self.undo_log)
            return True
        except Exception as ex:
            tkinter.messagebox.showerror("Error", "Failed to save " + filename + ": " + str(ex))
            return False

    def do_open(self, filename):
        try:
            with open(filename, "rb") as stream:
                code = stream.read()
            encoding = self.encoding.get()
            if encoding == "auto":
                try:
                    code = code.decode("utf-8")
                except UnicodeError:
                    code = code.decode("jelly")
            else:
                code = code.decode(encoding)
            self.load_file(code)
            self.saved = True
            self.modified.set(False)
            self.filename.set(filename)
            return True
        except Exception as ex:
            tkinter.messagebox.showerror("Error", "Failed to open " + filename + ": " + str(ex))
            return False

    def encoding_changed(self):
        if self.saved and tkinter.messagebox.askyesno("Encoding changed", "Would you like to reload the file?", default=tkinter.messagebox.YES, parent=self.ui):
            if not self.save_unsaved("reloading"):
                return
            self.do_open(self.filename.get())

    def code_changed(self):
        code = self.code.get()
        if self.prev_code != code:
            if not self.keep_undo:
                self.redo_log.clear()
                self.undo_log.append(self.prev_code)
                self.update_undo()
            self.modified.set(True)
            self.prev_code = code
            self.update_visualization()

    def update_visualization(self):
        try:
            visualized = visualize_code(self.code.get())
        except Exception:
            traceback.print_exc()
            return
        if visualized:
            visualized = PhotoImage(visualized)
            self.visualizer.configure(image=visualized)
            self.visualizer.image = visualized

    def insert_changed(self):
        self.insert_entries = entries = insert.find_inserts(self.insert_text.get())
        for label in self.insert_labels:
            label.grid_forget()
        for label in self.insert_desc_labels:
            label.grid_forget()
        self.insert_labels.clear()
        self.insert_desc_labels.clear()
        for i, entry in enumerate(entries):
            newlabel = tkinter.Label(self.insert_area, text=entry["name"], wraplength=40, bg="#ffffff", anchor=tkinter.W, justify=tkinter.LEFT, font=self.font)
            newlabel.grid(row=i + 1, column=0, sticky=tkinter.W + tkinter.E + tkinter.N + tkinter.S)
            self.insert_labels.append(newlabel)
            newdesclabel = tkinter.Label(self.insert_area, text=entry["desc"], wraplength=0, bg="#ffffff", anchor=tkinter.W, justify=tkinter.LEFT, font=self.font)
            newdesclabel.grid(row=i + 1, column=1, sticky=tkinter.W + tkinter.E)
            self.insert_desc_labels.append(newdesclabel)
        self.insert_selected.set(0 if self.insert_entries else -1)

    def insert_select_changed(self):
        selected = self.insert_selected.get()
        for i, entry in enumerate(self.insert_entries):
            color = "#ffffff" if i != selected else "#0078d7"
            self.insert_labels[i].configure(bg=color)
            self.insert_desc_labels[i].configure(bg=color, wraplength=0 if i != selected else 350)

    def update_title(self):
        self.ui.title(self.filename.get() + ("*" if self.modified.get() else "") + " \u2013 JellyIDE " + VERSION)

    def running_changed(self):
        state = tkinter.DISABLED if self.running.get() else tkinter.NORMAL
        self.run_menu.entryconfig("Run", state=state)
        self.run_btn.configure(state=state)
        self.run_menu.entryconfig("Debug", state=state)
        self.debug_btn.configure(state=state)

        state = tkinter.NORMAL if self.running.get() else tkinter.DISABLED
        self.run_menu.entryconfig("Stop", state=state)
        self.stop_btn.configure(state=state)

        state = tkinter.DISABLED if not self.running.get() or self.debug_state.get() != DebugState.running else tkinter.NORMAL
        self.run_menu.entryconfig("Pause", state=state)
        self.pause_btn.configure(state=state)

        state = tkinter.NORMAL if self.debug_state.get() != DebugState.running else tkinter.DISABLED
        self.run_menu.entryconfig("Resume", state=state)
        self.resume_btn.configure(state=state)
        self.run_menu.entryconfig("Step", state=state)
        self.step_btn.configure(state=state)
        self.run_menu.entryconfig("Step to return", state=state)

    def update_undo(self):
        self.modified.set(len(self.undo_log) != self.saved_state)
        self.edit_menu.entryconfig("Undo", state=tkinter.NORMAL if self.undo_log else tkinter.DISABLED)
        self.edit_menu.entryconfig("Redo", state=tkinter.NORMAL if self.redo_log else tkinter.DISABLED)

    def save_unsaved(self, action=""):
        if not self.modified.get():
            return True
        action = " before " + action if action else ""
        text = "Save changes to " + self.filename.get() + action + "?"
        result = tkinter.messagebox.askyesnocancel("Save changes?", text, default=tkinter.messagebox.CANCEL, parent=self.ui)
        if result:
            return self.save_file()
        return result is not None

    def load_file(self, code):
        self.prev_code = code
        self.code.set(code)
        self.undo_log.clear()
        self.redo_log.clear()
        self.saved_state = 0
        self.update_undo()
        self.update_visualization()

    def new_file(self):
        if not self.save_unsaved("creating new file"):
            return
        self.load_file(HELLO_WORLD)
        self.saved = False
        self.modified.set(False)
        self.filename.set(UNTITLED)

    def open_file(self):
        if not self.save_unsaved("opening"):
            return
        filename = tkinter.filedialog.askopenfilename(filetypes=FILE_FILTER)
        if not filename:
            return
        self.do_open(filename)

    def save_file(self):
        if not self.saved:
            return self.save_file_as()
        return self.do_save(self.filename.get())

    def save_file_as(self):
        filename = tkinter.filedialog.asksaveasfilename(filetypes=FILE_FILTER, defaultextension=".jelly", initialfile=self.filename.get())
        if not filename:
            return False
        return self.do_save(filename)

    def undo(self):
        if not self.undo_log:
            return
        self.keep_undo = True
        self.redo_log.append(self.code.get())
        self.code.set(self.undo_log.pop())
        self.keep_undo = False
        self.update_undo()

    def redo(self):
        if not self.redo_log:
            return
        self.keep_undo = True
        self.undo_log.append(self.code.get())
        self.code.set(self.redo_log.pop())
        self.keep_undo = False
        self.update_undo()

    def run_program(self):
        self.debugger.resume()
        self.do_run()

    def debug_program(self):
        self.debugger.pause()
        self.do_run()

    def do_run(self):
        if self.running.get():
            return
        self.running.set(True)
        self.runner = RunnerThread(self, self.code.get())
        self.runner.start()
        self.console_window.deiconify()
        self.console_window.lift()

    def pause_debug(self):
        self.debugger.pause()

    def resume_debug(self):
        self.debugger.resume()

    def step_debug(self):
        self.debugger.step()

    def step_to_return(self):
        self.debugger.step_to_return()

    def stop_program(self):
        self.debugger.stop()

    def insert_code(self):
        if not self.insert_shown:
            self.code_box.update_idletasks()
            x, y, _, textheight = self.code_box.bbox(self.code_box.index(tkinter.INSERT))
            boxy = self.toolbar.winfo_height()
            winx = self.ui.winfo_rootx()
            winy = self.ui.winfo_rooty()
            self.insert_selected.set(-1)
            self.insert_text.set("")
            self.insert_area.deiconify()
            self.insert_area.geometry("400x400+%d+%d" % (winx + x, winy + boxy + y + textheight))
            self.insert_box.focus_set()
            self.insert_shown = True

    def select_insert(self, delta):
        if not self.insert_entries:
            return
        current = self.insert_selected.get()
        current += delta
        if current < 0:
            current = len(self.insert_entries) - 1
        elif current >= len(self.insert_entries):
            current = 0
        self.insert_selected.set(current)

    def hide_insert(self):
        if self.insert_shown:
            self.insert_area.withdraw()
            self.code_box.focus_set()
            self.insert_shown = False

    def do_insert(self):
        current = self.insert_selected.get()
        if not self.insert_shown or current == -1:
            return
        entry = self.insert_entries[current]
        self.hide_insert()
        self.code_box.insert(tkinter.INSERT, entry["add"]())

    def output_callback(self, output):
        text = "".join(char if ord(char) <= 0xffff else "\\U%08x" % ord(char) for char in output)
        text = wrap_lines_for_tkinter(text)
        self.console.configure(state=tkinter.NORMAL)
        self.console.insert(tkinter.END, text)
        self.console.configure(state=tkinter.DISABLED)
        self.console.see(tkinter.END)

    def set_debug_state(self, state):
        self.debug_state.set(state)

    def run_finished(self, end=""):
        self.running.set(False)
        self.runner = None
        self.output_callback(end + "\n--- DONE ---\n")

    def select_all(self):
        self.code_box.tag_add("sel", "1.0", tkinter.END)
        return "break"


def wrap_lines_for_tkinter(text):
    """Tkinter seems to die with long line wraps, so try to overcome that"""
    MAX_LINE_LENGTH = 256
    output = ""
    for line in text.split("\n"):
        if output != "":
            output += "\n"
        while len(line) > MAX_LINE_LENGTH:
            output += line[:MAX_LINE_LENGTH] + "\n"
            line = line[MAX_LINE_LENGTH:]
        output += line
    return output


class RunnerThread(threading.Thread):
    def __init__(self, ide, code, args=None):  # TODO: args
        super().__init__(daemon=True)
        self.ide = ide
        self.links = interpreter.parse_code(code.replace("\n", "¶"))
        self.args = args or []
        ide.debugger.runner = self
        ide.debugger.start()

    def run(self):
        try:
            self.ide.debugger.output(interpreter.variadic_chain(self.links[-1] if self.links else [], self.args))
            self.ide.run_finished()
        except StopRunning:
            self.ide.run_finished()
        except JellyException as ex:
            self.ide.run_finished("--- ERROR ---\n" + format_jelly_exception(ex))
        except Exception as ex:
            self.ide.run_finished("".join(["--- ERROR ---\n"] + traceback.format_exception(type(ex), ex, ex.__traceback__)).rstrip("\n"))
